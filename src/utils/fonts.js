export const Fonts={
    Regular:'AlegreyaSans-Regular',
    Bold:'AlegreyaSans-Bold',
    Light:'AlegreyaSans-Light',
    Thin:'AlegreyaSans-Thin',
}