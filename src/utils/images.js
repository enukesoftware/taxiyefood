export const Images={
    step1_icon:require('../assets/img/icon1.png'),
    step2_icon:require('../assets/img/icon2.png'),
    step3_icon:require('../assets/img/icon3.png'),
    step4_icon:require('../assets/img/icon4.png'),
    ic_next_arrow:require('../assets/img/ic_next_arrow.png'),
    logo_search:require('../assets/img/logosearch.png'),
    ic_marker_pin:require('../assets/img/ic_marker_pin.png'),
    ic_search:require('../assets/img/ic_search.png'),
    ic_current_location:require('../assets/img/ic_current_location.png'),
    ic_location_arrow:require('../assets/img/ic_location_arrow.png'),
    ic_cross:require('../assets/img/ic_cross.png'),
    ic_back_ios:require('../assets/img/ic_back_ios.png'),
    ic_back_android:require('../assets/img/ic_back_android.png'),
    ic_my_location:require('../assets/img/ic_my_location.png'),
    info_background:require('../assets/img/info_background.png'),
    resto_logo:require('../assets/img/resto_logo.png'),
    close:require('../assets/img/close.png'),
    world:require('../assets/img/world.png'),
    double_arrow:require('../assets/img/double-arrow.png'),
    left_arrow:require('../assets/img/left-arrow.png'),
    person_white:require('../assets/img/person_white.png'),
    ic_plus:require('../assets/img/ic_plus.jpg'),
    ic_minus:require('../assets/img/ic_minus.jpg'),
    cancel:require('../assets/img/cancel.png'),
    gift:require('../assets/img/gift.png'),
    calender:require('../assets/img/calender.png'),
    clock:require('../assets/img/clock.png'),
    pizza:require('../assets/img/pizza.jpg'),
    no_restaurant:require('../assets/img/no_restaurant.png'),
    ic_distance_location:require('../assets/img/ic_distance_location.png'),
    ic_delivery:require('../assets/img/ic_delivery.png'),
    ic_delivery_man:require('../assets/img/ic_delivery_man.png'),
    ic_filter:require('../assets/img/ic_filter.png'),
    ic_search_white:require('../assets/img/ic_search_white.png'),
    ic_menu_drawer:require('../assets/img/ic_menu_drawer.png'),
    ic_settings:require('../assets/img/ic_settings.png'),
    ic_avatar:require('../assets/img/ic_avatar.png'),
    ic_envelope:require('../assets/img/ic_envelope.png'),
    ic_mobile:require('../assets/img/ic_mobile.png'),
    ic_cart_white:require('../assets/img/ic_cart_white.png'),
    ic_cart_red:require('../assets/img/ic_cart_red.png'),
    ic_address:require('../assets/img/ic_address.png'),
    ic_restaurants:require('../assets/img/ic_restaurants.png'),
    ic_deals:require('../assets/img/ic_deals.png'),
    ic_my_profile:require('../assets/img/ic_my_profile.png'),
    ic_orders:require('../assets/img/ic_orders.png'),
    ic_change_password:require('../assets/img/ic_change_password.png'),
    ic_about_us:require('../assets/img/ic_about_us.png'),
    ic_about_us_white:require('../assets/img/ic_about_us_white.png'),
    ic_contact_us:require('../assets/img/ic_contact_us.png'),
    ic_logout:require('../assets/img/ic_logout.png'),
    phone:require('../assets/img/phone.png'),
    ic_delivery_man_8080:require('../assets/img/ic_delivery_man_8080.png'),
    ic_location_8080:require('../assets/img/ic_location_8080.png'),
    ic_tickets:require('../assets/img/ic_tickets.png'),
    ic_hashtag:require('../assets/img/ic_hashtag.png'),
    ic_location:require('../assets/img/ic_location.png'),
    ic_state:require('../assets/img/ic_state.png'),
    ic_flag:require('../assets/img/ic_flag.png'),
    ic_house:require('../assets/img/ic_house.png'),
    ic_email:require('../assets/img/ic_email.png'),
    ic_person:require('../assets/img/ic_person.png'),
    ic_password:require('../assets/img/ic_password.png'),
    ic_person_blue:require('../assets/img/ic_person_blue.png'),
    ic_edit_white:require('../assets/img/ic_edit_white.png'),
    ic_guests:require('../assets/img/ic_guests.png'),
    ic_add_white:require('../assets/img/ic_add_white.png'),
    ic_location_white:require('../assets/img/ic_location_white.png'),
    ic_deliveryman:require('../assets/img/ic_deliveryman.png'),
    ic_play:require('../assets/img/ic_play.png'),
    ic_preparing_disabled:require('../assets/img/ic_preparing_disabled.png'),
    ic_preparing_enabled:require('../assets/img/ic_preparing_enabled.png'),
    ic_check_linecolor:require('../assets/img/ic_check_linecolor.png'),
    ic_check_white:require('../assets/img/ic_check_white.png'),
    ic_distance_location_linecolor:require('../assets/img/ic_distance_location_linecolor.png'),
    ic_distance_location_white:require('../assets/img/ic_distance_location_white.png'),
    ic_delivery_man_linecolor:require('../assets/img/ic_delivery_man_linecolor.png'),
    ic_delivery_man_white:require('../assets/img/ic_delivery_man_white.png'),
    bg:require('../assets/img/bg.png'),
    ic_star:require('../assets/img/ic_star.png'),
    ic_clock:require('../assets/img/ic_clock.png'),
    ic_rupee:require('../assets/img/ic_rupee.png'),
    ic_dollar:require('../assets/img/ic_dollar.png'),
    ic_close_white:require('../assets/img/ic_close_white.png'),
    ic_check_black:require('../assets/img/ic_check_black.png'),
    ic_booking_history:require('../assets/img/ic_booking_history.png'),
    ic_phonecall_white:require('../assets/img/ic_phonecall_white.png'),
    ic_big_cart:require('../assets/img/ic_big_cart.png'),
    ic_marker:require('../assets/img/ic_marker.png'),
    ic_delivery_boy:require('../assets/img/ic_delivery_boy.png'),
    ic_delete:require('../assets/img/ic_delete.png'),
    no_product:require('../assets/img/no_product.png'),
    
      //ic_big_cart:require('../assets/img/ic_big_cart.png'),
    
}

