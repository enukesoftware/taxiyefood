import {Platform, StyleSheet } from 'react-native';
import { Fonts } from '../../utils/fonts'
import { Constants } from '../../utils'


export default StyleSheet.create({

    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle:{
        fontSize: 20,
        color:Constants.color.white,
        marginLeft:15,
    },
    loginForm: {
        width: '100%',
        paddingHorizontal: 20,
        flex:1
    },
    imgBack: {
        width: '100%',
        height: '100%'
    },
    textAmountButton: {
        color: Constants.color.black,
        fontSize: 14,
        fontWeight: '500'
    },
    textAmountProcessing: {
        color: Constants.color.primary,
        fontSize: 14,
        fontWeight: '500'
    },
    viewItems: {
        width: '75%',
        paddingVertical: 5,
    },
    textItems: {
        fontSize: 14,
       // color: Constants.color.drakgray,
    },
    viewInputRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        width: '90%',

    },
    textItemName: {
        color: Constants.color.black,
        marginTop: 3,
        marginBottom: 10,
        fontWeight: '500',
        fontSize: 16
    },
    imgLogo: {
        width: 100,
        height: 100
    },
    viewRadio: {
        width: '12%',
        height: 100,
        padding: 5,
        alignItems: "center",
        justifyContent: "center",
    },
    mainCard: {
        margin: 10,
       // shadowColor: Constants.color.gray, // IOS
        shadowOffset: { height: 1, width: 1 }, // IOS
        shadowOpacity: 0.5, // IOS
        shadowRadius: 1, //IOS
        borderRadius: 10,
        backgroundColor: 'white',
        elevation: 4, // Android

       /*  margin: 10,
        borderRadius: 5,
        backgroundColor: 'white',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
        elevation: 5 */
    },
    buttonAmount: {
        backgroundColor: Constants.color.gray,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Constants.color.black,
    },
    buttonStatus: {
        marginHorizontal: 5,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Constants.color.primary,
    }




})
