import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Platform, DeviceEventEmitter, View, Dimensions, TouchableOpacity, Image, Text, SafeAreaView, StatusBar } from 'react-native';
import styles from './styles'
import FirebaseDatabase from '../../firebase-database'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import MapView, { Marker, PROVIDER_GOOGLE, AnimatedRegion, MapViewDirections, Permissions, Location } from 'react-native-maps'
import { directionApi } from '../../services/APIService'
import PolylineDecoder from '@mapbox/polyline'
import DriverComponent from './driver'


const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const latDelta = 0.10
const longDelta = latDelta * ASPECT_RATIO
// const latDelta = 0.015 * 5
// const longDelta = 0.0121 * 5
let order = null

class TrackDriver extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super()
        interval = 0;

    }

    state = {
        initialized: false,
        deliverLatitude: 0,
        deliverLongitude: 0,
        mLattitude: 0,
        mLongitude: 0,
        oldLattitude: 0,
        oldLongitude: 0,
        pickUpLatitude: 0,
        pickUpLongitude: 0,
        orderStatus: "0",
        polylineList: [],
        tempPolylineList: [],
        currentBearing: 0,
        newBearing: 0,
        oldBearing: 0,
        driverLatitude: 0,
        driverLongitude: 0,
    }


    componentWillMount() {

        const { navigation } = this.props;
        const order = navigation.getParam('order', null);
        this.order = order
        // console.log("ORDER_DRIVER:"+JSON.stringify(order))
    }

    componentDidMount() {

        // DeviceEventEmitter.addListener(Constants.EVENTS.liveLocation, this.updateLocation.bind(this))
        // if (this.props.liveLocationData && this.props.liveLocationData.orderNumber == this.order.order_number && !this.state.initialized) {
        //     this.setState(prevState => ({
        //         deliverLatitude: this.props.liveLocationData.deliverLatitude,
        //         deliverLongitude: this.props.liveLocationData.deliverLongitude,
        //         mLattitude: this.props.liveLocationData.mLattitude,
        //         mLongitude: this.props.liveLocationData.mLongitude,
        //         oldLattitude: prevState.mLattitude == 0 ? this.props.liveLocationData.mLattitude : prevState.mLattitude,
        //         oldLongitude: prevState.mLongitude == 0 ? this.props.liveLocationData.mLongitude : prevState.mLongitude,
        //         pickUpLatitude: this.props.liveLocationData.pickUpLatitude,
        //         pickUpLongitude: this.props.liveLocationData.pickUpLongitude,
        //         orderStatus: this.props.liveLocationData.orderStatus,
        //         initialized: true,
        //         newBearing: this.getBearing(0, 0, this.props.liveLocationData.mLattitude, this.props.liveLocationData.mLongitude),
        //         oldBearing: 0
        //     }), () => {
        //         this.callGoogleMapDirectionApi()
        //         // this.animateDriver()
        //     })

        // }

        DeviceEventEmitter.addListener(Constants.EVENTS.liveLocation, (data1) => {
            // alert(JSON.stringify(data))
            data1 = JSON.stringify(data1)
            let data = JSON.parse(data1)
            // console.log("CHANGE_LOC1:" + data.mLattitude + " " + data.mLongitude)
            // try{
            //     if(data.mLattitude && data.mLattitude>0){
            //         data["mLattitude"] = data.mLattitude.toFixed(6);
            //     }
            //     if(data.mLongitude && data.mLongitude>0){
            //         data["mLongitude"] = data.mLongitude.toFixed(6);
            //     }
                
            // }catch(error){

            // }
            console.log("CHANGE_LOC2:" + data.mLattitude + " " + data.mLongitude)

            this.setState(prevState => ({
                deliverLatitude: data.deliverLatitude,
                deliverLongitude: data.deliverLongitude,
                mLattitude: data.mLattitude,
                mLongitude: data.mLongitude,
                oldLattitude: prevState.mLattitude == 0 ? data.mLattitude : prevState.mLattitude,
                oldLongitude: prevState.mLongitude == 0 ? data.mLongitude : prevState.mLongitude,
                pickUpLatitude: data.pickUpLatitude,
                pickUpLongitude: data.pickUpLongitude,
                orderStatus: data.orderStatus,
                newBearing: this.getBearing(prevState.oldLattitude, prevState.oldLongitude, data.mLattitude, data.mLongitude),
                oldBearing: prevState.newBearing,
                initialized: true,
            }), () => {
                this.callGoogleMapDirectionApi()
                // this.animateDriver()
            })

        })


        // orderRef = firebase.database().ref('locations' + '/' + this.order.driver_id + '/' + this.order.order_number)
        //     console.log("REF:"+orderRef)
        //     orderRef.once('value', (childSnapshot) => {
        //         console.log("SNAPSHOT0:" + JSON.stringify(childSnapshot))
        //     })
        //     orderRef.on('value', (childSnapshot) => {
        //         console.log("SNAPSHOT1:" + JSON.stringify(childSnapshot))
        //     })
        //     orderRef.on('child_added', (childSnapshot) => {
        //         console.log("SNAPSHOT2:" + JSON.stringify(childSnapshot))
        //     })

        //     orderRef.on('child_changed', (childSnapshot) => {
        //         console.log("SNAPSHOT3:" + JSON.stringify(childSnapshot))
        //     })
    }


    

    componentWillUnmount() {
        DeviceEventEmitter.removeAllListeners();
        window.clearInterval(this.interval);
        // orderRef.off('value');
    }


    //  render() {
    //      return (
    //           <View>
    //              <FirebaseDatabase driverId={112} orderNumber={1549004340} />
    //            </View>
    //      );
    //  };


    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }

    // Converts from degrees to radians.
    toRadians(degrees) {
        return degrees * Math.PI / 180;
    };

    // Converts from radians to degrees.
    toDegrees(radians) {
        return radians * 180 / Math.PI;
    }


    getBearing(startLat, startLng, destLat, destLng) {
        startLat = this.toRadians(startLat);
        startLng = this.toRadians(startLng);
        destLat = this.toRadians(destLat);
        destLng = this.toRadians(destLng);

        y = Math.sin(destLng - startLng) * Math.cos(destLat);
        x = Math.cos(startLat) * Math.sin(destLat) -
            Math.sin(startLat) * Math.cos(destLat) * Math.cos(destLng - startLng);
        brng = Math.atan2(y, x);
        brng = this.toDegrees(brng);
        // console.log("BEARING:"+brng)
        return (brng + 360) % 360;
    }

    

    animatePolyline = () => {
        // this.interval = setInterval(() => this.animatePolylineStart(), 100);
    };

    animatePolylineStart = () => {
        console.log("PATH1:" + this.interval + "     " + JSON.stringify(this.state.polylineList))

        // while(this.state.polylineList.length>0){
        if (this.state.polylineList.length > 0) {
            const Direction = this.state.polylineList;
            const polylineList = [
                ...Direction.slice(0, this.state.polylineList.length - 1)
            ];
            this.setState({ polylineList });
            // } else {
            //     this.setState({ polylinePath: [] })
        } else {
            clearInterval(this.interval)
            // const newCoordinate = {
            //     latitude: this.state.mLattitude + (Math.random() - 0.5) * (0.015 * 5),
            //     longitude: this.state.mLongitude + (Math.random() - 0.5) * (0.0121 * 5),
            // };

            // if (Platform.OS === 'android') {
            //     if (this.driverMarker) {
            //         this.driverMarker._component.animateMarkerToCoordinate(newCoordinate, 500);
            //     }
            // } else {
            //     this.state.coordinate.timing(newCoordinate).start();
            // }


        }
    };


    animateDriver = () => {
        // console.log("ANIMATE_DRIVER:")
        // console.log("LAT:"+this.state.mLattitude+" LONG:"+this.state.mLongitude)
        // this.driverInterval = setInterval(() => this.animateDriverStart(), 50);
        this.oldBearing = this.state.oldBearing
        this.newBearing = this.state.newBearing
        this.currentBearing = this.oldBearing
        let startTimeInMillis = (new Date).getTime();
        let duration = 1000
        this.setState({
            currentBearing:this.newBearing
        },()=>{
            this.setState(prevState => ({
                driverLatitude: prevState.mLattitude,
                driverLongitude: prevState.mLongitude,
                polylineList: this.state.tempPolylineList
            }))
        })
        // console.log("TIME_IN_MILLIS:"+startTimeInMillis)
        // this.driverInterval = setInterval(() => this.animateDriverStart(startTimeInMillis, duration), 10);
    }

    animateDriverStart = (startTimeInMillis, duration) => {
        let elapsed = (new Date).getTime() - startTimeInMillis
        let animationTime = elapsed / duration
        let rot = animationTime * this.newBearing + (1 - animationTime) * this.oldBearing;
        this.currentBearing = (-rot > 180) ? (rot / 2) : rot

        this.setState({
            currentBearing: this.currentBearing
        })
        if (animationTime >= 1.0) {
            // Post again 16ms later.
            clearInterval(this.driverInterval)
            this.setState(prevState => ({
                driverLatitude: prevState.mLattitude,
                driverLongitude: prevState.mLongitude,
                polylineList: this.state.tempPolylineList
            }))
        }
    }


    callGoogleMapDirectionApi = () => {
        if(!this.props.internet){
                    alert(strings.message_lno_internet)
                    return
                }
        let url = Constants.URL.googleMapDirectionApiUrl + "mode=driving&transit_routing_preference=less_driving&origin=" + this.state.mLattitude + "," + this.state.mLongitude + "&destination=" + this.state.deliverLatitude + "," + this.state.deliverLongitude + "&key=" + Constants.GOOGLE_API_KEY
        console.log("URL:" + url)
        directionApi(url).then(res => {

            // console.log("DIRECTION_API_RES:" + JSON.stringify(res))
            if (res && res.routes) {
                res.routes.forEach(route => {
                    let polyLine = route.overview_polyline.points
                    let array = PolylineDecoder.decode(polyLine)
                    let polylist = array.map((point) => {
                        return {
                            latitude: point[0],
                            longitude: point[1]
                        }
                    })
                    // console.log("POLYLINE:"+JSON.stringify(polylist))
                    this.setState({
                        tempPolylineList: polylist
                    }, () => {
                        // this.animatePolyline()
                        this.animateDriver()
                    })
                });
                if(res.routes.length == 0){
                    this.animateDriver()
                }
            }
            // if (res && res.success && res.restaurant) {
            //     this.setState({
            //         restaurantPages: res.restaurant,
            //     })
            // }
            // if (res && res.success && res.restaurant && res.restaurant.data && res.restaurant.data.length > 0) {

            //     this.setState({
            //         restNoData: false,
            //         restaurantDataList: res.restaurant.data,
            //         filteredRestList: res.restaurant.data,
            //         restaurantLoaderIsVisible: false,
            //     },()=>{
            //         if(res.admin && res.admin.length>0){
            //             AsyncStorage.setItem(Constants.STORAGE_KEY.adminData, JSON.stringify(res.admin[0]));

            //         }
            //     })
            // } else {
            //     this.setState({
            //         restNoData: true,
            //         restaurantLoaderIsVisible: false,
            //     })
            // }

            //  this.props.savehotellist(res)
        }).catch(err => {
            // this.setState({
            //     restNoData: true,
            //     restaurantLoaderIsVisible: false,
            // })
            setTimeout(() => {
                if (err) {
                    // err = JSON.parse(err)
                    if (err != '') {
                        alert(err);
                    }
                }
            }, 100);
        });
    }

    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }

    renderDriverMarker() {

    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '20%'
                }
                }>
                    {this.renderBackButton()}


                </View>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: "center",
                    width: '60%'
                }
                }>
                    <TextBold title={strings.search_location} textStyle={styles.textTitle} />
                </View>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '20%'
                }
                }>

                </View>
            </View>

        )
    }

    renderMapAndroid() {
        return (
            <MapView
                ref={(mapView) => { this.mapView = mapView; }}
                style={styles.map}
                provider={PROVIDER_GOOGLE}
                showsMyLocationButton={false}
                showsUserLocation={true}
                region={{
                    latitudeDelta: latDelta,
                    longitudeDelta: longDelta,
                    latitude: Number(this.state.mLattitude),
                    longitude: Number(this.state.mLongitude)
                }}
            >
                <Marker
                    coordinate={{
                        latitudeDelta: latDelta,
                        longitudeDelta: longDelta,
                        latitude: this.state.pickUpLatitude,
                        longitude: this.state.pickUpLongitude
                        // latitude: 28.4458173,
                        // longitude: 77.0679864,
                    }}
                >
                    <View style={{ alignItems: 'center' }}>
                        <TextBold title={this.order.restaurant.name} textStyle={styles.mapText} />
                        <Image source={Images.ic_marker} style={{ height: 25, width: 20, marginTop: 5 }} />
                    </View>

                </Marker>

                <Marker
                    coordinate={{
                        latitudeDelta: latDelta,
                        longitudeDelta: longDelta,
                        latitude: this.state.deliverLatitude,
                        longitude: this.state.deliverLongitude
                        // latitude: 28.4458173,
                        // longitude: 77.0679864,
                    }}
                // title="My Location"
                // description={this.state.storeDetails.}
                >
                    <View style={{ alignItems: 'center' }}>
                        <TextBold title={this.order.customer.first_name} textStyle={styles.mapText} />
                        <Image source={Images.ic_marker} style={{ height: 25, width: 20, marginTop: 5 }} />
                    </View>

                </Marker>

                <DriverComponent driver={{
                    location: {
                        latitudeDelta: latDelta,
                        longitudeDelta: longDelta,
                        latitude: this.state.driverLatitude,
                        longitude: this.state.driverLongitude
                    },
                    bearing: this.state.currentBearing
                }} />

                {
                    this.state.polylineList.length > 0 ?
                        <MapView.Polyline
                            coordinates={this.state.polylineList}
                            strokeWidth={5}
                            strokeColor={Constants.color.primary} />
                        : null
                }
            </MapView>
        )
    }

    renderMapIos() {
        return (
            <MapView
                ref={(mapView) => { this.mapView = mapView; }}
                style={styles.map}
                mapType={Platform.OS == "android" ? "none" : "standard"}
                provider={PROVIDER_GOOGLE}
                showsMyLocationButton={false}
                // zoomEnabled={true}
                // zoomControlEnabled={true}
                region={{
                    // altitudeDelta: 0.015 * 5,
                    latitudeDelta: latDelta,
                    longitudeDelta: longDelta,
                    latitude: this.state.mLattitude,
                    longitude: this.state.mLongitude
                    // latitude: 28.4458173,
                    // longitude: 77.0679864,
                }}
            >
                <Marker
                    coordinate={{
                        altitudeDelta: latDelta,
                        longitudeDelta: longDelta,
                        latitude: this.state.pickUpLatitude,
                        longitude: this.state.pickUpLongitude
                        // latitude: 28.4458173,
                        // longitude: 77.0679864,
                    }}
                >
                    <View style={{ alignItems: 'center' }}>
                        <TextBold title={this.order.restaurant.name} textStyle={styles.mapText} />
                        <Image source={Images.ic_marker} style={{ height: 25, width: 20, marginTop: 5 }} />
                    </View>

                </Marker>

                <Marker
                    coordinate={{
                        altitudeDelta: latDelta,
                        longitudeDelta: longDelta,
                        latitude: this.state.deliverLatitude,
                        longitude: this.state.deliverLongitude
                        // latitude: 28.4458173,
                        // longitude: 77.0679864,
                    }}
                // title="My Location"
                // description={this.state.storeDetails.}
                >
                    <View style={{ alignItems: 'center' }}>
                        <TextBold title={this.order.customer.first_name} textStyle={styles.mapText} />
                        <Image source={Images.ic_marker} style={{ height: 25, width: 20, marginTop: 5 }} />
                    </View>

                </Marker>


                <DriverComponent driver={{
                    location: {
                        altitudeDelta: latDelta,
                        longitudeDelta: longDelta,
                        latitude: this.state.driverLatitude,
                        longitude: this.state.driverLongitude
                    },
                    bearing: this.state.currentBearing
                }} />

                {
                    this.state.polylineList.length > 0 ?
                        <MapView.Polyline
                            coordinates={this.state.polylineList}
                            strokeWidth={5}
                            strokeColor={Constants.color.primary} />
                        : null
                }
                {/* Animating polyline */}
                {/* <AnimatingPolylineComponent Direction={this.state.polylineList} /> */}

            </MapView>
        )
    }

    renderMap() {
        return (
            <View style={{ flex: 1, width: '100%' }}>
                {
                    Platform.OS == 'android'
                        ? this.renderMapAndroid()
                        : this.renderMapIos()
                }
            </View>
        )

    }

    render() {
        // const { region, address, addressForAutoSearch } = this.state
        return (

            <View style={{ width: '100%', flex: 1, flexDirection: 'column' }}>
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                <FirebaseDatabase  driverId={this.order.driver_id?this.order.driver_id:0} orderNumber={this.order.order_number} />
                {this.renderToolbar()}

                <View style={styles.container}>

                    {/* --------------- Map View --------------- */}

                    {this.renderMap()}


                </View>
            </View>

        );
    };
}

function mapStateToProps(state) {
    return {
        liveLocationData: state.liveLocationData,
        internet:state.internet
    }
}

export default connect(mapStateToProps, {})(TrackDriver)
