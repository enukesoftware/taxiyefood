import {Platform, StyleSheet } from 'react-native';
import { Constants } from '../../utils'
import { Fonts } from '../../utils/fonts'


export default StyleSheet.create({

    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle:{
        fontSize: 20,
        color:Constants.color.white,
        marginLeft:15,
    },
    loginForm: {
        padding: 30,
        width: '100%',
        flex: 8,
    },
    textInput: {
        fontSize: 16,
        height: 40,
        fontFamily: Fonts.Regular,
        width: '100%'
    },
    inputLayout: {
        width: '80%',
        marginHorizontal: 10
    },
    iconLeft: {
        height: 25,
        width: 25,
        margin: 5
    },
    viewInput: {
        flexDirection: "row",
        alignItems: 'flex-end',
    },
    imgBack: {
        width: '100%',
        height: '100%'
    },
    touchOpacity: {
        flex: 0.8,
        backgroundColor: Constants.color.primary,
        justifyContent: "center",
        alignItems: "center"
    },
    textButtonAdd: {
        color: Constants.color.white,
        fontSize: 16
    },


})
