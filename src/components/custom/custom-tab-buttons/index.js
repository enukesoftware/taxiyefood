import React, { Component } from 'react'
import { Platform, StyleSheet, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../../custom/text'
import { dashboardTabSelected } from '../../../redux/actions'

class CustomTabButtons extends Component {

    constructor() {
        super()
    }

    onSelect = (selected) => {
        // this.setState({
        //     tabSelected: selected
        // })

        this.props.dashboardTabSelected(selected)
        this.props.onTabChange(selected)
        // console.log('SELECTED:', this.state.tabSelected)
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => this.onSelect(0)}
                    style={[styles.touchableCommon, this.props.tabSelected == 0 ? styles.touchableSelectedLeft : styles.touchableUnselectedLeft]}>
                    <TextBold title={strings.restaurant}
                        textStyle={[styles.tabText, this.props.tabSelected == 0 ? styles.tabTextSelectedLeft : styles.tabTextUnselectedLeft]} />
                </TouchableOpacity>

                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => this.onSelect(1)}
                    style={[styles.touchableCommon, this.props.tabSelected == 1 ? styles.touchableSelectedRight : styles.touchableUnselectedRight]}>
                    <TextBold title={strings.home_coocked_food}
                        textStyle={[styles.tabText, this.props.tabSelected == 1 ? styles.tabTextSelectedRight : styles.tabTextUnselectedRight]} />

                </TouchableOpacity>
            </View>
        );
    };
}

function mapStateToProps(state) {
    return {
        tabSelected: state.dashboardTabSelected,
    }
}

export default connect(mapStateToProps,{dashboardTabSelected})(CustomTabButtons)



const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 15,
        marginBottom: 10
    },
    touchableCommon: {
        width: 130,
        paddingVertical: 6,
        borderWidth: 1,
        borderColor: Constants.color.primary,
        justifyContent: 'center'
    },
    tabText: {
        fontSize: Constants.fontSize.SmallXX,
        textAlign: 'center',
    },
    touchableSelectedLeft: {
        backgroundColor: Constants.color.primary,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,

    },
    touchableUnselectedLeft: {
        backgroundColor: 'white',
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
    },
    touchableSelectedRight: {
        backgroundColor: Constants.color.primary,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
    },
    touchableUnselectedRight: {
        backgroundColor: 'white',
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
    },
    tabTextSelectedLeft: {
        color: Constants.color.fontWhite,
    },
    tabTextUnselectedLeft: {
        color: Constants.color.fontBlack,
        opacity: 0.7
    },
    tabTextSelectedRight: {
        color: Constants.color.fontWhite,
    },
    tabTextUnselectedRight: {
        color: Constants.color.fontBlack,
        opacity: 0.7
    },
})