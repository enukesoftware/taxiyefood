import React, { Component } from 'react'
import { Platform, Picker, SafeAreaView, TouchableHighlight, Image, View, TouchableOpacity, Text, ActivityIndicator, Alert } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import NavigationServices from '../../services/NavigationServices';


class OrderConfirmation extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            succesfull: true,
            key:""
        }
    }

    
    componentDidMount() {
        const { navigation } = this.props;
        const navigateFrom = navigation.getParam('NavigateFrom', '');

        //console.log('addessJson:'+navigateFrom)
        if (navigateFrom == 'Checkout') {
            let key = navigation.getParam('KEY', '');
            // console.log('addessJson:' + JSON.stringify(key))
            this.setState({
                key:key
            })
        }

    }

    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }



    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={strings.order_confirmation} textStyle={styles.textTitle}/>

                </View>
                {/* <View style={{ flexDirection: 'row',  width: '10%' }}>
                    <TouchableOpacity activeOpacity={0.8}
                    >
                        <Image source={Images.ic_close_white}
                            style={{ marginRight: 15, height: 25, width: 25 }}
                        ></Image>
                    </TouchableOpacity>
                </View> */}
            </View>

        )
    }






    render() {

        console.log('Checkout_KEY_ORDERv:' + JSON.stringify(this.state.key))

        return (
            <View style={{ flex: 1, backgroundColor: Constants.color.white }} >
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>

                <View style={{ flex: 1, }}>
                {this.renderToolbar()}

                <View style={styles.loginForm}>
                    <View style={{ flex: 2.5, width: '100%', alignItems: "center" }}>
                        <Image
                            resizeMode='contain' style={{ width: '100%', height: '100%' }}
                            source={Images.ic_deliveryman}></Image>
                    </View>
                    <View style={{ flex: 7.5 }}>

                        <TextBold title={strings.order_text} textStyle={styles.textConfirmation}/>
                        <View style={styles.viewText}>
                            <TextRegular title={strings.ordercode_text} textStyle={styles.textLine}/>
                            <TextBold title={this.state.key==""? null: this.state.key.data.order_number} textStyle={styles.textData}/>
                        </View>
                        <View style={styles.viewText}>
                            <TextRegular title={strings.orderreceive_text} textStyle={styles.textLine}/>
                            <TextBold title={this.state.key==""? null: this.state.key.data.restaurant.delivery_time+" "+strings.minutes} textStyle={styles.textData}/>
                        </View>

                        <TextRegular title={strings.enquiry_text} textStyle={styles.textContact}/>
                        <View style={styles.viewEmailRow}>
                            <Image source={Images.ic_email} style={styles.iconLeft}></Image>
                            <TextRegular title={this.state.key==""? null: this.state.key.data.admin[0].email} textStyle={styles.textEmail}/>
                        </View>
                        <View style={styles.viewEmailRow}>
                            <Image source={Images.phone} style={styles.iconLeft}></Image>
                            <TextRegular title={this.state.key==""? null: this.state.key.data.admin[0].contact_number } textStyle={styles.textEmail}/>
                        </View>

                        <TouchableOpacity
                                    onPress={() => {
                                      this.props.navigation.navigate("Dashboard")
                                    }}
                                    style={styles.touchOpacity}>
                                    <TextRegular title={strings.ok} textStyle={styles.textButtonAdd}/>
                                </TouchableOpacity>
                    </View>


                </View>
                </View>
            </View>

        );
    }
}




export default OrderConfirmation







