import React, { Component } from 'react'
import { Platform, StyleSheet, View, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { Rating, AirbnbRating } from 'react-native-ratings';
import NavigationServices from '../../services/NavigationServices'



class RestaurantItem extends Component {

    constructor() {
        super()
    }

    renderItemUnavailableView() {
        if (this.props.item.is_open != '1') {
            return (
                <View style={{
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                }}>
                    <View style={{
                        // position: 'absolute',
                        width: '100%',
                        height: '100%',
                        backgroundColor: 'gray',
                        opacity: 0.8,
                        borderRadius: 5,
                        // shadowOffset: { width: 1, height: 1 },
                        // shadowOpacity: 0.2,
                        // elevation: 5
                    }}>
                    </View>

                    <TextBold title={strings.currently_unavailable} textStyle={{ fontSize: Constants.fontSize.NormalXX, alignSelf: 'center', position: 'absolute', top: '40%', color: Constants.color.black }}></TextBold>

                </View>
            )
        } else {
            return (
                <View></View>
            )
        }

    }

    onItemClick = () => {
        if (this.props.item.is_open === '1') {
            NavigationServices.navigate("RestaurantMenu", { item: this.props.item })
        } else {
            // NavigationServices.navigate("RestaurantMenu", { item: this.props.item })
        }
    }

    onBookTableClick = () =>{
        if(this.props.isLogin){
            NavigationServices.navigate("TableBooking",{restDetail:this.props.item})
        }else{
            NavigationServices.navigate('Login', { previousPage: 'Dashboard' })
        }
        
    }

    renderBookTableView = () => {
        if (this.props.tabSelected == 0) {
            return (
                <TouchableOpacity style={styles.bookTableTouchable}
                onPress={()=>this.onBookTableClick()}
                >
                    <TextBold title={strings.book_table} textStyle={{ color: Constants.color.fontWhite, fontSize: Constants.fontSize.SmallXX }} />
                </TouchableOpacity>
            )
        } else {
            return
        }
    }

    render() {
        let menus = ''
        for (let i = 0; i < this.props.item.menus.length > 0; i++) {
            if (i == 0) {
                menus = menus + this.props.item.menus[i].menu_name
            } else {
                menus = menus + ', ' + this.props.item.menus[i].menu_name
            }
        }
        let restImage = Images.no_restaurant
        if (this.props.item.image && this.props.item.image.location && this.props.item.image.location != '') {
            restImage = { uri: this.props.item.image.location }
        }
        return (
            <TouchableOpacity style={styles.container}
                onPress={() => this.onItemClick()}
            >



                <View style={styles.upperView}>

                    {/* --------------- Image and Info View --------------- */}
                    <View style={styles.imageInfoView}>
                        <View >
                            <Image source={restImage} style={styles.restImage}></Image>
                        </View>

                        {/* --------------- Info View --------------- */}
                        <View style={styles.infoView}>
                            <TextBold title={this.props.item.name} numberOfLines={1} ellipsizeMode='tail' textStyle={styles.infoRestName} />
                            <TextBold title={this.props.item.street} numberOfLines={4} ellipsizeMode='tail' textStyle={styles.infoRestdAddress} />
                            <TextRegular title={menus} numberOfLines={2} ellipsizeMode='tail' textStyle={styles.infoRestdAddress} />
                        </View>
                    </View>

                    {/* --------------- Rating View --------------- */}
                    <View style={styles.ratingView}>

                        <View style={{ alignSelf: 'flex-end' }}>
                            <View style={styles.showRating}>
                                <TextRegular title={parseInt(this.props.item.rating)} textStyle={styles.ratingText} />
                            </View>

                            <Rating
                                readonly={true}
                                type='custom'
                                onFinishRating={() => parseInt(this.props.item.rating)}
                                imageSize={10}
                                ratingCount={5}
                                startingValue={parseInt(this.props.item.rating)}
                                ratingBackgroundColor={Constants.color.gray}
                                ratingColor={Constants.color.rating}
                                style={{ marginTop: 5 }}
                            />

                            {this.renderBookTableView()}
                        </View>

                    </View>

                    {/* --------------- Rating View --------------- */}

                </View>

                <View style={{ height: 1, width: '100%', backgroundColor: Constants.color.gray }} />

                {/* Bottom View */}
                <View style={styles.bottomView}>
                    <View style={[styles.bottomViewBlock, { flex: 4 }]}>
                        <Image source={Images.ic_distance_location} style={styles.blockImage}></Image>
                        <TextRegular title={strings.distance} textStyle={styles.blockLabel} />
                        <TextRegular title={this.props.item.distance + ' km'} textStyle={[styles.blockValue, { width: '35%' }]} />
                    </View>
                    <View style={styles.blockDivider} />
                    <View style={[styles.bottomViewBlock, { flex: 5.5 }]}>
                        <Image source={Images.ic_delivery} style={styles.blockImage}></Image>
                        <TextRegular title={strings.delivery_charge} textStyle={styles.blockLabel} />
                        <TextRegular title={'$ ' + this.props.item.delivery_charge} textStyle={[styles.blockValue, { width: '30%' }]} />
                    </View>
                    <View style={styles.blockDivider} />
                    <View style={[styles.bottomViewBlock, { flex: 2.5 }]}>
                        <Image source={Images.ic_delivery_man} style={styles.blockImage}></Image>
                        <TextRegular title={this.props.item.delivery_time + ' ' + strings.minutes} textStyle={[styles.blockValue, { color: Constants.color.primary, width: '65%' }]} />
                    </View>
                </View>





                {/* --------------- Unavailable View --------------- */}
                {this.renderItemUnavailableView()}

                {/* </View> */}
            </TouchableOpacity>
        );
    };
}

function mapStateToProps(state) {
    return {
        tabSelected: state.dashboardTabSelected,
        isLogin:state.isLogin
        // state
    }
}

export default connect(mapStateToProps, {})(RestaurantItem)
// export default RestaurantItem

export const styles = StyleSheet.create({
    container: {
        margin: 10,
        borderRadius: 5,
        backgroundColor: 'white',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
        elevation: 5
    },
    upperView: {
        flexDirection: 'row'
    },
    imageInfoView: {
        flex: 7.8,
        padding: 10,
        flexDirection: 'row',
    },
    infoView: {
        marginLeft: 10,
    },
    infoRestName: {
        fontSize: Constants.fontSize.NormalXX,
        color: Constants.color.fontBlack,
    },
    infoRestdAddress: {
        fontSize: Constants.fontSize.SmallXXX,
        color: Constants.color.fontBlack,
        opacity: 0.8
    },
    infoRestCusines: {
        fontSize: Constants.fontSize.SmallXXX,
        color: Constants.color.fontBlack
    },
    ratingView: {
        flex: 4,
        justifyContent: 'center',
        paddingHorizontal: 10,
    },
    ratingText: {
        color: Constants.color.fontWhite,
        fontSize: Constants.fontSize.SmallXXX
    },
    showRating: {
        width: 24,
        height: 24,
        borderRadius: 24 / 2,
        backgroundColor: Constants.color.rating,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    restImage: {
        height: 60,
        width: 60,
        borderRadius: 60 / 2,
        backgroundColor: 'grey',
        alignSelf: 'center',
        resizeMode: 'cover',
    },
    bottomView: {
        flexDirection: 'row',
    },
    bottomViewBlock: {
        flex: 0.4,
        flexDirection: 'row',
        paddingHorizontal: 5,
        paddingVertical: 10,
        alignItems: 'center'
    },
    blockImage: {
        width: 20,
        height: 20
    },
    blockDivider: {
        height: '100%',
        width: 1,
        backgroundColor: Constants.color.gray
    },
    blockLabel: {
        fontSize: Constants.fontSize.SmallX,
        opacity: 0.5,
        paddingLeft: 5
    },
    blockValue: {
        fontSize: Constants.fontSize.SmallX,
        paddingLeft: 5,
        color: Constants.color.fontBlack,
        flexWrap: 'wrap',
        // backgroundColor:'red'
    },
    bookTableTouchable: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 3,
        paddingHorizontal: 5,
        backgroundColor: Constants.color.primary,
        marginTop: 5,
        borderRadius: 5,
    }

})



                                
        