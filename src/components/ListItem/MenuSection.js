
import React, { Component } from 'react'
import { Platform, StyleSheet, View } from 'react-native';
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import MenuItem from './MenuItem'
import MenuItemModify from './MenuItemModify'
import { ScrollView } from 'react-native-gesture-handler';

class MenuSection extends Component {

    constructor() {
        super()
    }



    render() {
        return (
            <View style={styles.container}>
                <View>
                    <TextBold title={this.props.value.menu_name} textStyle={styles.headerText} />
                </View>
                <View>
                    {this.props.type === "Modify" ?

                        this.props.value.products.map((item, key) => (

                            <MenuItemModify
                                key={key}
                                value={item}
                                menuName={this.props.value.menu_name}
                                restDetail={this.props.restDetail}
                            />
                        ))
                        :
                        this.props.value.products.map((item, key) => (
                            <MenuItem
                                key={key}
                                value={item}
                                menuName={this.props.value.menu_name}
                                restDetail={this.props.restDetail}
                            />
                        ))

                    }
                </View>

            </View>
        );
    };
}

export default MenuSection

const styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        paddingHorizontal: 15,
        marginBottom: 10,
        borderRadius: 5,
        backgroundColor: 'white',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
        elevation: 5
    },
    headerText: {
        fontSize: Constants.fontSize.NormalXX,
        opacity: 0.8,
        marginBottom: 5,
    }
})

