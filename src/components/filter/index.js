import React, { Component } from 'react'
import { Platform, SafeAreaView, TouchableHighlight, View, Image, Text, AsyncStorage } from 'react-native';
import styles from './styles'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import CheckBox from 'react-native-check-box'
import { Rating, AirbnbRating } from 'react-native-ratings';
import { TouchableOpacity } from 'react-native-gesture-handler';
import NavigationServices from '../../services/NavigationServices';


class Filter extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            isDealsChecked: false,
            isOpenRestaurantsChecked: false,
            isVegChecked: false,
            isNonVegChecked: false,
            selectedCuisineList: [],
            selectedRating: 0,

        }
    }

    componentDidMount() {
        AsyncStorage.getItem(Constants.STORAGE_KEY.filterRestaurants, (error, result) => {
            if (error) {
                console.log("ERROR:" + JSON.stringify(error))
            }
            else {
                if (result) {
                    result = JSON.parse(result)
                    this.setState({
                        isDealsChecked: result.deals === 'true' ? true : false,
                        isOpenRestaurantsChecked: result.open === 'true' ? true : false,
                        isVegChecked: result.veg === 'true' ? true : false,
                        isNonVegChecked: result.non_veg === 'true' ? true : false,
                        selectedCuisineList: result.cuisines,
                        selectedRating: parseInt(result.rating),
                    })
                }
            }
        })
    }

    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }



    renderCrossButton() {
        return (
            <TouchableOpacity activeOpacity={0.8}
                onPress={() => this.onBackClick()}>
                <Image source={Images.ic_close_white}
                    style={{ marginLeft: 15, height: 25, width: 25, }}
                ></Image>
            </TouchableOpacity>
        )
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '80%'
                }
                }>
                    {this.renderCrossButton()}
                    <TextBold title={strings.title_filter} textStyle={styles.textTitle} />

                </View>
                {<View style={{ flexDirection: 'row', width: '20%', alignItems: "center" }}>
                    <TouchableOpacity activeOpacity={0.8}
                        onPress={() => this.onResetClick()}>

                        <TextRegular title={strings.menu_reset} textStyle={styles.TextResetNavigation} />

                    </TouchableOpacity>
                </View>}
            </View>

        )
    }

    //function to be pass to next screen and get result
    onCuisineSelect = data => {
        console.log("CUISINE_DATA:" + JSON.stringify(data))
        this.setState({
            selectedCuisineList: data.selectedCuisineList,
        })



    };

    onRatingSelect = (rating) => {
        console.log("RATING:" + rating)
        this.setState({
            selectedRating: rating
        })
    }

    onResetClick = () => {
        this.setState({
            isDealsChecked: false,
            isOpenRestaurantsChecked: false,
            isVegChecked: false,
            isNonVegChecked: false,
            selectedCuisineList: [],
            selectedRating: 0,
        })
    }

    onApplyFilterClick = () => {
        if (this.state.selectedRating == 0
            && !this.state.isDealsChecked
            && !this.state.isOpenRestaurantsChecked
            && !this.state.isVegChecked
            && !this.state.isNonVegChecked
            && this.state.selectedCuisineList.length == 0) {
            AsyncStorage.removeItem(Constants.STORAGE_KEY.filterRestaurants)

            const { navigation } = this.props;
            navigation.goBack();
            navigation.state.params.onFilterSelect({
                filters: null,
            });
        } else {
            const filterRestaurants = {
                rating: this.state.selectedRating.toString(),
                deals: this.state.isDealsChecked.toString(),
                open: this.state.isOpenRestaurantsChecked.toString(),
                veg: this.state.isVegChecked.toString(),
                non_veg: this.state.isNonVegChecked.toString(),
                cuisines: this.state.selectedCuisineList,
            }

            AsyncStorage.setItem(Constants.STORAGE_KEY.filterRestaurants, JSON.stringify(filterRestaurants))
            const { navigation } = this.props;
            navigation.goBack();
            navigation.state.params.onFilterSelect({
                filters: filterRestaurants,
            });
        }


    }


    render() {
        let cuisines = strings.cuisines;
        for (let i = 0; i < this.state.selectedCuisineList.length > 0; i++) {
            if (i == 0) {
                cuisines = this.state.selectedCuisineList[i]
            } else {
                cuisines = cuisines + ', ' + this.state.selectedCuisineList[i]
            }
        }
        return (
            <View style={{ flex: 1 }} >
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                {this.renderToolbar()}
                <View style={{ flex: 8 }}>

                    <TextRegular title={strings.sort_by} textStyle={[styles.textInfo, styles.textHead,]} ></TextRegular>


                    {/* --------------- Rating View --------------- */}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                        <TextRegular title={strings.rating} textStyle={[styles.textInfo, styles.textData]} ></TextRegular>

                        <View >
                            {/* <View style={styles.showRating}>
                                <TextRegular title={parseInt(3)} textStyle={styles.ratingText} />
                            </View> */}

                            <Rating
                                // readonly={true}
                                type='custom'
                                onFinishRating={(rating) => this.onRatingSelect(rating)}
                                imageSize={15}
                                ratingCount={5}
                                startingValue={this.state.selectedRating}
                                ratingBackgroundColor={Constants.color.gray}
                                ratingColor={Constants.color.rating}
                            // style={{ marginTop: 10 }}
                            />
                        </View>

                    </View>

                    {/* --------------- Rating View --------------- */}

                    <TextRegular title={strings.quick_filters} textStyle={[styles.textInfo, styles.textHead,]} ></TextRegular>

                    <View style={styles.rowCheckbox}>

                        <CheckBox
                            style={{ flex: 1, padding: 10, }}
                            checkBoxColor={Constants.color.primary}
                            onClick={() => {
                                this.setState({
                                    isDealsChecked: !this.state.isDealsChecked
                                })
                            }}
                            isChecked={this.state.isDealsChecked}
                            rightText={strings.side_menu_deals}
                            rightTextStyle={styles.rightTextStyle}
                        />

                        <CheckBox
                            style={{ flex: 2, padding: 10, }}
                            checkBoxColor={Constants.color.primary}
                            onClick={() => {
                                this.setState({
                                    isOpenRestaurantsChecked: !this.state.isOpenRestaurantsChecked
                                })
                            }}
                            isChecked={this.state.isOpenRestaurantsChecked}
                            rightText={strings.open_restaurants}
                            rightTextStyle={styles.rightTextStyle}
                        />
                    </View>
                    <View style={styles.rowCheckbox}>

                        <CheckBox
                            style={{ flex: 1, padding: 10, }}
                            checkBoxColor={Constants.color.primary}
                            onClick={() => {
                                this.setState({
                                    isVegChecked: !this.state.isVegChecked
                                })
                            }}
                            isChecked={this.state.isVegChecked}
                            rightText={strings.veg} />

                        <CheckBox
                            style={{ flex: 2, padding: 10, }}
                            checkBoxColor={Constants.color.primary}
                            onClick={() => {
                                this.setState({
                                    isNonVegChecked: !this.state.isNonVegChecked
                                })
                            }}
                            isChecked={this.state.isNonVegChecked}
                            rightText={strings.non_veg} />
                    </View>

                    <TextRegular title={strings.cuisines} textStyle={[styles.textInfo, styles.textHead,]} ></TextRegular>

                    <TouchableOpacity style={[styles.rowCheckbox, { paddingVertical: 5 }]}
                        onPress={() => NavigationServices.navigate("CuisinesSelection", {
                            onCuisineSelect: this.onCuisineSelect,
                            cuisineList: this.state.selectedCuisineList
                        })}>
                        <Image resizeMode={'center'} source={Images.world} style={{ marginLeft: 5, flex: 1 }}></Image>
                        <TextRegular textStyle={[styles.textData, { flex: 8 }]} title={cuisines}> </TextRegular>
                        <Image resizeMode={'center'} source={Images.double_arrow} style={{ flex: 1 }}></Image>
                    </TouchableOpacity>


                </View>
                <View style={{ flex: .8, }} >
                    <TouchableOpacity style={{
                        height: '100%',
                        width: '100%',
                        backgroundColor: Constants.color.primary,
                        alignItems: 'center',
                        justifyContent: 'center',
                        padding:12,
                    }}
                        onPress={() => this.onApplyFilterClick()}>
                        <TextBold textStyle={styles.TextResetNavigation} title={strings.apply_filter} />
                    </TouchableOpacity>
                </View>
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
            </View>
        )
    }

}

export default Filter







