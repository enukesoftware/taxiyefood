import React, { Component } from 'react'
import { Platform, Animated, Card, ScrollView, TouchableHighlight, View, ImageBackground, Dimensions, TouchableOpacity, Image, Text, SafeAreaView, StatusBar } from 'react-native';

import styles from './styles'
import { connect } from 'react-redux'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Geocoder from 'react-native-geocoding'
import { locationSelected } from '../../redux/actions'
import NavigationService from '../../services/NavigationServices'
import { Rating, AirbnbRating } from 'react-native-ratings';


class RestaurantDetails extends Component {

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      dat: [strings.mon, strings.tue, strings.wed, strings.thurs, strings.fri, strings.sat, strings.sun],
      scrollY: new Animated.Value(0.01),
      loading: false,
      screenHeight: Dimensions.get("window").height,
      screenWidth: Dimensions.get("window").width,
      restDetail: null

    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    const restDetail = navigation.getParam('restDetail', null);

    this.setState({
      restDetail: restDetail,
    }, () => {
      this.updatePinLocationOnMap(restDetail.latitude, restDetail.longitude)
    })
    console.log(restDetail)
  }

  xyz = () => {

    return (<View>
      {
        this.state.restDetail == null ?
          <View></View> :
          <View>
            {this.state.restDetail.open_time.map((item, index) =>
              (<View key={index} style={styles.weekRow}>
                <View style={{ width: '40%' }} >
                  <TextBold title={this.state.dat[item.weekday - 1]} textStyle={[styles.textInfo, { color: '#808080', marginHorizontal: 20, marginVertical: 5, }]} />
                </View>
                <View style={{ width: '60%' }} >
                  <TextBold title={item.open + '-' + item.closing} textStyle={[styles.textInfo, { color: '#808080', marginHorizontal: 20, marginVertical: 5, }]} />
                </View></View>))}
          </View>
      }
    </View>)
  }
  updatePinLocationOnMap = (latitude, longitude) => {
    console.log("LOCATION-" + latitude + ' - ' + longitude)
    let camera = {
      center: {
        latitude: parseInt(latitude),
        longitude: parseInt(longitude),
      },
      zoom: 10,
    }
    this.mapView.animateCamera(camera, 500)

  }
  userRatings = () => {
    return (<View>
      {
        this.state.restDetail == null ?
          <View></View> :
          <View>
            {this.state.restDetail.review.map((item, index) =>
              (
                <View style={styles.ratingContainer}>
                  <View style={styles.ratingInnerContainer}>
                    <Rating
                      readonly={true}
                      type='custom'
                      onFinishRating={() => parseInt(item.rating)}
                      imageSize={12}
                      ratingCount={5}
                      startingValue={parseInt(item.rating)}
                      ratingBackgroundColor={Constants.color.gray}
                      ratingColor={Constants.color.rating}
                      style={{ marginTop: 25, backgroundColor: Constants.color.gray }}
                    />
                    <TextRegular title={item.date} textStyle={[styles.textInfo, styles.ratingUsername]} />
                  </View>
                  <View style={styles.ratingInnerContainer}>
                    {item.customer.profile_image == "" ? <Image source={Images.info_background} style={[styles.ratingUserProfileImage, { marginTop: 15 }]} /> : <Image source={{ uri: item.customer.profile_image }} style={[styles.ratingUserProfileImage, { marginTop: 15 }]} />}
                    <TextRegular title={item.customer.first_name + '' + item.customer.last_name} textStyle={[styles.textInfo, styles.ratingUsername]} />
                    <TextRegular title={item.review} textStyle={[styles.textInfo, styles.ratingComment]} />
                  </View>
                </View>))}
          </View>
      }
    </View>)
  }
  deTails = () => {
    const restDetail = this.state.restDetail
    let paymentMethod = ''
    let reviewRating = ''
    let address = ''

    if (restDetail) {
      paymentMethod = (restDetail.payment_method && restDetail.payment_method === 'cash') ? strings.cod_text : ''
      if (restDetail.review_rating && restDetail.review_rating.length > 0) {
        restDetail.review_rating.forEach(element => {
          reviewRating = reviewRating + '(' + element.total_reviews + ')'
        });
      } else {
        reviewRating = strings.no_review
      }

      let floor = (restDetail.floor && restDetail.floor != '') ? restDetail.floor + ' ' + strings.floor_text + ', ' : ''
      let street = (restDetail.street && restDetail.street != '') ? restDetail.street + ', ' : ''
      let city = ''
      if (restDetail.city && restDetail.city.name && restDetail.city.name != '') {
        city = restDetail.city.name
      }
      address = floor + street + city
    }
    return (<View >
      <TextBold title={strings.opening_hour} textStyle={[styles.textInfo, styles.textHead]} />

      {this.xyz()}
      <View style={styles.viewHead}></View>

      <TextBold title={strings.payment_text} textStyle={[styles.textInfo, styles.textHead]} />
      <TextBold title={paymentMethod} textStyle={[styles.textInfo, styles.textData]} />

      <View style={styles.viewHead}></View>
      <View style={{ flexDirection: 'row' }}>
        <TextBold title={strings.rating_text} textStyle={[styles.textInfo, styles.textHead]} />
        {reviewRating != strings.no_review ? <TextBold title={reviewRating} textStyle={[styles.textInfo, styles.textData, { marginTop: 25, marginHorizontal: 0 }]} />
          : <View></View>}
      </View>
      {reviewRating == strings.no_review ? <TextBold title={reviewRating} textStyle={[styles.textInfo, styles.textData]} />
        : this.userRatings()}

      <View style={styles.viewHead}></View>

      <TextBold title={address} textStyle={[styles.textInfo, styles.textHead]} />

      <View style={{ height: 250, width: '90%', margin: "5%", backgroundColor: 'white', borderRadius: 10, shadowColor: 'black', shadowRadius: 5, shadowOpacity: 0.5, elevation: 10 }}>
        {this.state.restDetail != null ?
          <View>{Platform.OS == 'android' ? <MapView
            ref={(mapView) => { this.mapView = mapView; }}
            style={{
              width: '100%', height: 250, justifyContent: 'center', alignItems: 'center',
              borderWidth: 1, borderRadius: 10, borderColor: "white",
            }}
            showsMyLocationButton={false}
            showsUserLocation={false}

            provider={PROVIDER_GOOGLE}
            region={{
              latitude: parseInt(this.state.restDetail.latitude),
              longitude: parseInt(this.state.restDetail.longitude),
              latitudeDelta: 0.015 * 5,
              longitudeDelta: 0.0121 * 5,
            }}
          ><Marker
              coordinate={{
                latitude: parseInt(this.state.restDetail.latitude),
                longitude: parseInt(this.state.restDetail.longitude),
                latitudeDelta: 0.015 * 5,
                longitudeDelta: 0.0121 * 5,
              }}
              title={this.state.restDetail.name}
            />

          </MapView> : <MapView
            ref={(mapView) => { this.mapView = mapView; }}
            style={{
              width: '100%', height: 250, justifyContent: 'center', alignItems: 'center',
              borderWidth: 1, borderRadius: 10, borderColor: "white",
            }}
            showsMyLocationButton={false}
            showsUserLocation={false}

            provider={PROVIDER_GOOGLE}
            region={{
              latitude: parseInt(this.state.restDetail.latitude),
              longitude: parseInt(this.state.restDetail.longitude),
              altitudeDelta: 0.015 * 5,
              longitudeDelta: 0.0121 * 5,

            }}
          ><Marker
                coordinate={{
                  latitude: parseInt(this.state.restDetail.latitude),
                  longitude: parseInt(this.state.restDetail.longitude),
                  altitudeDelta: 0.015 * 5,
                  longitudeDelta: 0.0121 * 5,
                }}
                title={this.state.restDetail.name}
              // description={this.state.storeDetails.}
              />

            </MapView>}</View>
          : <View></View>}



      </View>
    </View>)
  }

  render() {
    let translateY = this.state.scrollY.interpolate({
      inputRange: [0, Constants.Screen.heignt / 3],
      outputRange: [0, -((Constants.Screen.heignt / 3) - (Constants.AppConstant.statusBarHeight + (Platform.OS === Constants.PLATFORM.ios) ?44:56))],
      extrapolate: 'clamp',
    });/* 

    let scale = this.state.scrollY.interpolate({
      inputRange: [0, Constants.Screen.heignt / 3],
      outputRange: [1, 0.5],
      extrapolate: 'clamp',
    });

    let textTranslateY = this.state.scrollY.interpolate({
      inputRange: [0, 90],
      outputRange: [0, 20],
      extrapolate: 'clamp',
    }); */

    let animatBackgroundColor = this.state.scrollY.interpolate({
      inputRange: [0, Constants.Screen.heignt / 3],
      outputRange: ['transparent', Constants.color.primary],
      extrapolate: 'clamp'
    });/* 

    let animateImageHeight = this.state.scrollY.interpolate({
      inputRange: [0, Constants.Screen.heignt / 3],
      outputRange: [0, -((Constants.Screen.heignt / 3) - (Constants.AppConstant.statusBarHeight + 44))],
      extrapolate: 'clamp',
    });

    let animateLinearOpacity = this.state.scrollY.interpolate({
      inputRange: [0, Constants.AppConstant.statusBarHeight + 44],
      outputRange: [0, 0.5],
      extrapolate: 'clamp',
    })

    let animateEditBtn = this.state.scrollY.interpolate({
      inputRange: [0, (Constants.Screen.heignt / 3) - 25],
      outputRange: [0, -(((Constants.Screen.heignt / 3) - 25) - (Constants.AppConstant.statusBarHeight + 44))],
      extrapolate: 'clamp',
    });

    let animateEditScale = this.state.scrollY.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    }) */

    const restDetail = this.state.restDetail
    let restName = ''
    let address = ''
    let restImage = Images.no_restaurant


    if (restDetail) {
      restName = (restDetail.name) ? restDetail.name : ''

      let floor = (restDetail.floor && restDetail.floor != '') ? restDetail.floor + ' ' + strings.floor_text + ', ' : ''
      let street = (restDetail.street && restDetail.street != '') ? restDetail.street + ', ' : ''
      let city = ''
      if (restDetail.city && restDetail.city.name && restDetail.city.name != '') {
        city = restDetail.city.name
      }
      address = floor + street + city

      restImage = (restDetail.image && restDetail.image.location && restDetail.image.location != '') ? { uri: restDetail.image.location } : Images.no_restaurant
    }

    return (
      <View style={styles.container}>
        <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
        <Animated.ScrollView
          scrollEventThrottle={20}
          showsVerticalScrollIndicator={false}
          onScroll={Animated.event([
            { nativeEvent: { contentOffset: { y: this.state.scrollY } } },
          ])}
          contentContainerStyle={{ paddingTop: Constants.Screen.heignt / 3 }}
          collapsable={true}
          bounces={false}
          style={{ flex: 1 }}>

          {this.deTails()}
        </Animated.ScrollView>

        <Animated.Image style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          height: Constants.Screen.heignt / 3,
          backgroundColor: Constants.color.primary,
          alignItems: 'center',
          justifyContent: 'flex-end',
          // paddingBottom: 10,
          transform: [{ translateY }],
        }}
          resizeMode='cover'
          source={Images.info_background}
        />
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          height: Constants.Screen.heignt / 3,
          backgroundColor: 'transparent',
          alignItems: 'center',
          justifyContent: 'center',
          // paddingBottom: 10,
          transform: [{ translateY }],
        }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%',marginTop: (Platform.OS === Constants.PLATFORM.ios) ?
            Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android}}>
            <View style={{ width: '30%', padding: 15, overflow: "hidden" }}>
              <Image style={{ width: 60, height: 60, borderRadius: 30, borderWidth: 2, borderColor: 'white' }} source={restImage} />
            </View>
            <View style={{ flexDirection: 'column', width: '60%' }}>
              <TextBold title={restName} textStyle={{ color: 'white', fontWeight: 'bold', fontSize: 22, width: 200 }}></TextBold>
              <TextBold title={address} textStyle={{ color: 'white', fontWeight: 'bold', fontSize: 16, width: 300 }} />
            </View>
          </View>
        </Animated.View>
        {/*  <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          height: Constants.Screen.heignt / 4,
          backgroundColor: 'transparent',
          //alignItems: 'center',
          justifyContent: 'flex-end',
          // paddingBottom: 10,
          transform: [{ translateY }],
        }}>
        
        </Animated.View> */}
        {/*  <Animated.View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            height: Constants.Screen.heignt / 4,
            backgroundColor: 'transparent',
            alignItems: 'center',
            justifyContent: 'flex-end',
            // paddingBottom: 10,
            transform: [{ translateY }],
          }}>
        </Animated.View> */}


        <Animated.View style={{
          flexDirection: 'row',
          top: 0,
          left: 0,
          right: 0,
          position: 'absolute',
          paddingTop: (Platform.OS === Constants.PLATFORM.ios) ?
            Constants.AppConstant.statusBarHeight : 0,
          height: (Platform.OS === Constants.PLATFORM.ios) ?
            Constants.TOOLBAR_HEIGHT.ios + Constants.AppConstant.statusBarHeight : Constants.TOOLBAR_HEIGHT.android,
          alignItems: 'center', justifyContent: 'center', backgroundColor: animatBackgroundColor
        }}>
          <Animated.View style={{ height: '100%', width: '20%', marginLeft: 5, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableHighlight style={{ height: 35, width: 35, }} underlayColor='transparent' onPress={() => {
              NavigationService.goBack();
            }}>
              <Image style={{ height: 35, width: 20 }} resizeMode='contain' source={Images.ic_back_ios} />
            </TouchableHighlight>
          </Animated.View>
          <Animated.View style={{ height: '100%', width: '80%', justifyContent: 'center', justifyContent: 'center' }}>
            <TextBold title={strings.restaurant_info} textStyle={{ fontSize: 20, color: 'white' }} />
          </Animated.View>

        </Animated.View>


        <StatusBar barStyle="light-content" />
      </View>
    );


  };
}



export default RestaurantDetails







