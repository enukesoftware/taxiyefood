import { StyleSheet } from 'react-native';
import { Constants,Fonts } from '../../utils'


export default StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent:'center',
    },
    activityIndicatorView:{
        height: '100%',
        width: '100%',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: '15%'
    },
    activityIndicatorWrapper:{
        // position:'absolute',
        // top:'35%',
        // left:'38%',
        backgroundColor: Constants.color.gray,
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 0.8,
    }
    
})