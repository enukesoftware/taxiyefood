import React, { Component } from 'react'
import { Platform, StyleSheet, View, FlatList } from 'react-native';
import styles from './styles'
import { connect } from 'react-redux'
import RestaurantItem from '../ListItem/RestaurantItem'


class RestaurantListPage extends Component {

    state = {
        restaurantDataList: [],
        homeCookedFoodDataList: []
    }

    renderRestaurantItem = () => {
        if (this.props.tabSelected == 0) {
            // if (this.props.selected == 0) {
            return (
                <View style={{ width: '100%' }}>
                    {/* <FlatList
                        // onRefresh={this.mRefresh}
                        // refreshing={this.state.isRefreshing}
                        data={this.state.restaurantDataList}
                        keyExtractor={item => item.id}
                        // ListHeaderComponent={this.renderHeader}
                        // onEndReached={this.onScrollHandler}
                         // onEndReachedThreshold={0.1}
                        renderItem={({ item }) => <RestaurantItem item={item} onPress={() => navigate('RestaurantMenu', item)} />}
                    /> */}
                    {/* <RestaurantItem />
                    <RestaurantItem /> */}
                </View>
            )
        } else {
            return
        }

    }

    renderHomeCookedItem = () => {
        if (this.props.tabSelected == 1) {
            // if (this.props.selected == 1) {
            return (
                <View style={{ width: '100%' }}>
                    <RestaurantItem />
                    {/* <RestaurantItem /> */}
                </View>
            )
        } else {
            return
        }

    }

    render() {
        return (
            <View>
                {this.renderRestaurantItem()}
                {this.renderHomeCookedItem()}
            </View>
        )
    }

}


function mapStateToProps(state) {
    // console.log("STATE:" + JSON.stringify(state))
    return {
        location: state.location,
        tabSelected: state.dashboardTabSelected,
    }
}

export default connect(mapStateToProps, {})(RestaurantListPage)

