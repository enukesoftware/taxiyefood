import {Platform, StyleSheet } from 'react-native';
import { Fonts } from '../../utils/fonts'
import { Constants } from '../../utils'


export default StyleSheet.create({

  
    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 5
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle: {
        fontSize: 20,
        color: Constants.color.white,
        marginLeft: 15,
    },
    loginForm: {
        width: '100%',
        flex:1
    },
    imgBack: {
        width: '100%',
        height: '100%'
    },
    textAmountButton: {
        color: Constants.color.black,
        fontSize: 14,
    },
    textAmountProcessing: {
        color: Constants.color.primary,
        fontSize: 14,
    },
    viewItems: {
        width: '75%',
        padding: 5,
    },
    textItems: {
        width: '50%',
        fontSize: 14,
        color:Constants.color.drakgray
    },
    viewInputRow: {
        flexDirection: 'row', 
        width: '100%'
    },
    textItemName: {
        color: Constants.color.black,
        marginTop: 3,
        marginBottom: 10,
        fontSize: 18,
    },
    imgLogo: {
        width: 80,
        height:80
    },
    viewImg: {
        width: '25%',
        height: 100,
        padding: 5,
        alignItems: "center",
        justifyContent: "center",
    },
    mainCard: {
        borderColor: Constants.color.white,
        borderBottomColor: Constants.color.gray,
        borderWidth: 1.5,
        padding: 2
    },
    buttonAmount: {
        backgroundColor: Constants.color.gray,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Constants.color.black,
    },
    buttonStatus: {
        marginHorizontal: 5,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Constants.color.primary,
    }




})
