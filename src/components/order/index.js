import React, { Component } from 'react'
import { Platform, TouchableOpacity, ActivityIndicator, Image, View, ImageBackground, Alert, PermissionsAndroid, DeviceEventEmitter } from 'react-native';
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import styles from './styles'
import { connect } from 'react-redux'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { SafeAreaView } from 'react-navigation';
import NavigationService from '../../services/NavigationServices'
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import { OrderDetailApi, OrderCancleApi } from '../../services/APIService'
import { openSettings, check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';


// let PermissionCallAndroid = false;

// async function requestPhonePermission() {
//     try {
//         const granted = await PermissionsAndroid.request(
//             PermissionsAndroid.PERMISSIONS.CALL_PHONE
//         );
//         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
//             PermissionCallAndroid = true;
//         } else {
//             PermissionCallAndroid = false;
//             alert('Provide Permission of Phone Call in Setting')
//         }
//     } catch (err) {
//         console.warn('error');
//     }
// }





class Order extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            order_number: "",
            isLoading: false,
            orderData: null,
            listIndex: null,
        }
        this.orderNotificationHandler = this.orderNotificationHandler.bind(this);
        this.showSettingsAlert = this.showSettingsAlert.bind(this);
        this.callOnNumber = this.callOnNumber.bind(this);

    }



    componentDidMount() {
        const { navigation } = this.props;
        const navigateFrom = navigation.getParam('previousPage', '');
        DeviceEventEmitter.addListener(Constants.EVENTS.orderNotification, this.orderNotificationHandler)
        //console.log('addessJson:'+navigateFrom)
        if (navigateFrom == 'Orders') {
            const order_number = navigation.getParam('order_number', '');
            const orderDataList = navigation.getParam('orderDataList', '');
            const index = navigation.getParam('index', '');

            if (order_number != "" || order_number != null) {
                this.setState({
                    order_number: order_number,
                    listIndex: index
                })
                this.callOrderDetailApi(order_number)
            }
        }
    }

    componentWillUnmount() {
        DeviceEventEmitter.removeListener(Constants.EVENTS.orderNotification, this.orderNotificationHandler)
    }

    orderNotificationHandler() {
        if (this.state.order_number && this.state.order_number != "") {
            this.callOrderDetailApi(this.state.order_number)
        }
    }

    callOnNumber = (number) => {
        if (Platform.OS == 'ios') {
            RNImmediatePhoneCall.immediatePhoneCall(number)
        } else {
            request(PERMISSIONS.ANDROID.CALL_PHONE).then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log(
                            'This feature is not available (on this device / in this context)',
                        );
                        alert('This feature is not available on this device')
                        break;
                    case RESULTS.DENIED:
                        console.log(
                            'The permission has not been requested / is denied but requestable',
                        );

                        break;
                    case RESULTS.GRANTED:
                        console.log('The permission is granted');
                        RNImmediatePhoneCall.immediatePhoneCall(number)
                        break;
                    case RESULTS.BLOCKED:
                        console.log('The permission is denied and not requestable anymore');
                        this.showSettingsAlert();
                        // openSettings().catch(() => console.warn('cannot open settings'))
                        break;
                }
            })
                .catch(error => {

                });
        }

    }

    showSettingsAlert = () => {
        Alert.alert("Permission", "To make phone call, allow App access to your phone call. Tap Settings > Permissions, and turn Phone on?",
            [
                {
                    text: "OK",
                    onPress: () => { openSettings().catch(() => console.warn('cannot open settings')) }
                },
                {
                    text: "Cancel",
                    onPress: () => { }
                },
            ])
    }

    callOrderDetailApi = (order_number) => {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }
        this.setState({
            isLoading: true,
        })

        let param = {
            order_number: order_number,
        }

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.orderDetail

        OrderDetailApi(url, param).then(res => {


            if (res && res.success) {
                console.log("ORDER-DETAIL:" + JSON.stringify(res.data))
                this.setState({
                    isLoading: false,
                    orderData: res.data
                })
                // alert(res.message);
            } else {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res && res.error) {
                        alert(res.error)
                    }
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err) + "catch");
                }
            }, 100);
        });

    }

    callOrderCancleApi = () => {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }
        this.setState({
            isLoading: true,
        })

        // console.log("ORDER-CANCLE2:" + this.state.order_number+"  "+this.state.orderData.user_id)

        let param = {
            order_id: this.state.order_number,
            user_id: this.state.orderData.user_id
        }

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.orderCancle

        OrderCancleApi(url, param).then(res => {

            console.log("ORDER-CANCLE:" + JSON.stringify(res))

            if (res && res.success) {
                // console.log("ORDER-DETAIL:" + JSON.stringify(res))
                this.setState({
                    isLoading: false,
                })
                this.callOrderDetailApi(param.order_id)
                // alert(res.message);
            } else {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res && res.error) {
                        alert(res.error)
                    }
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err) + "catch");
                }
            }, 100);
        });

    }



    onBackClick = () => {
        if (this.state.orderData && this.state.orderData.status) {
            const status = this.state.orderData.status
            const { navigation } = this.props;
            navigation.goBack();
            navigation.state.params.updateStatus({
                status: status,
                index: this.state.listIndex
            });
        } else {
            navigation.goBack();
        }

    }
    generateModifyBasketDataObject() {
        const orderData = this.state.orderData
        let products = []
        let total_price = 0
        let total_quantity = 0
        orderData.items.forEach(element => {

            total_price = total_price + parseInt(element.product_total_price);
            total_quantity = total_quantity + parseInt(element.product_quantity)
            // console.log('ItemsDETE:' + (JSON.stringify(element)))
            let addON = {};
            let addOnGrpTemp = [];
            let addOnGrp = [];
            let addONPrice = 0;
            if (element.addons_list != null) {
                element.addons_list.forEach(addOnElement => {
                    addOnGrpTemp.push(addOnElement.id)
                    addONPrice = addONPrice + parseInt(addOnElement.price)
                });

                addOnGrp = Array.from(new Set(addOnGrpTemp));

                addOnGrp.forEach(el => {
                    addON[el] = []
                })

                element.addons_list.forEach(addOnElement => {
                    addON[addOnElement.id].push(addOnElement)

                });


            }


            // console.log('ItemsaddON:' + (JSON.stringify(addON)))
            let obj = {
                'id': element.product_id,
                'name': element.product_name,
                'quantity': parseInt(element.product_quantity),
                'unitPrice': parseInt(element.product_unit_price),
                'productPrice': parseInt(element.product_total_price),
                'selectedAddOns': addON,
                'cost': parseInt(element.product_unit_price) - parseInt(addONPrice),
                'selectedAddOnsGroups': addOnGrp,
                'image': element.image ? element.image : null
            }
            products.push(obj)
        });

        const basketDataObject = {
            'restDetail': orderData.restaurant,
            'restId': orderData.restaurant_id,
            'totalQuantity': parseInt(total_quantity),
            'totalPrice': parseInt(total_price),
            "totalCgst": 0,
            "totalSgst": 0,
            'products': products
        }
        return basketDataObject


    }

    editClicked = () => {
        let obj = this.generateModifyBasketDataObject();
        //console.log('basketDataObject:' + (JSON.stringify(obj)))

        NavigationService.navigate('ModifyBasket', {
            NavigateFrom: 'Order', basketDataObject: obj,
            order_id: this.state.order_number,
        });
    }

    viewClicked = () => {
        NavigationService.navigate('ViewOrder', { NavigateFrom: 'Order', order_number: this.state.order_number });
    }

    cancleClicked = () => {
        Alert.alert(
            strings.cancel + " " + strings.order_title,
            strings.cancel_order_message,
            [
                //   {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                {
                    text: strings.no,
                    onPress: () => this.call,
                    style: 'cancel',
                },
                {
                    text: strings.yes,
                    onPress: () => this.callOrderCancleApi()
                },
            ],
            { cancelable: false },
        );
    }






    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={strings.order_title} textStyle={styles.textTitle} />

                </View>

            </View>

        )
    }

    tracker(s) {
        const status = parseInt(s)
        if (status != 6) {
            return (
                <View style={styles.weekRow}>
                    {/* Processing */}
                    <View style={styles.viewImagesEnabled}>
                        <Image style={{ width: '50%' }} resizeMode="contain" source={Images.ic_check_white}></Image>
                    </View>

                    {/* Preparing */}

                    {
                        status > 1 || status == 7 ?
                            <>
                                <View style={styles.venable}></View>
                                <View style={styles.viewImagesEnabled}>
                                    <Image style={{ width: '60%' }} resizeMode="contain"
                                        source={Images.ic_preparing_enabled}></Image>
                                </View>
                            </> :
                            <>
                                <View style={styles.vdisable}></View>
                                <View style={styles.viewImagesDisabled}>
                                    <Image style={{ width: '60%' }} resizeMode="contain"
                                        source={Images.ic_preparing_disabled}></Image>
                                </View>
                            </>
                    }
                    {/* On the Way */}

                    {
                        status > 3 && status != 7 ?
                            <>
                                <View style={styles.venable}></View>
                                <View style={styles.viewImagesEnabled}>
                                    <Image style={{ width: '65%' }} resizeMode="contain"
                                        source={Images.ic_distance_location_white}></Image>
                                </View>
                            </> :
                            <>
                                <View style={styles.vdisable}></View>
                                <View style={styles.viewImagesDisabled}>
                                    <Image style={{ width: '65%' }} resizeMode="contain"
                                        source={Images.ic_distance_location_linecolor}></Image>
                                </View>
                            </>
                    }
                    {/* Delivered */}

                    {
                        status > 4 && status != 7 ?
                            <>
                                <View style={styles.venable}></View>
                                <View style={styles.viewImagesEnabled}>
                                    <Image style={{ width: '60%' }} resizeMode="contain"
                                        source={Images.ic_delivery_man_white}></Image>
                                </View>
                            </> :
                            <>
                                <View style={styles.vdisable}></View>
                                <View style={styles.viewImagesDisabled}>
                                    <Image style={{ width: '60%' }} resizeMode="contain"
                                        source={Images.ic_delivery_man_linecolor}></Image>
                                </View>
                            </>
                    }

                </View>)
        }
        else {
            return (
                <View style={{ alignItems: "center", justifyContent: 'center', padding: 10 }}>
                    <Image source={Images.cancel} style={{ width: 64, height: 64, resizeMode: 'contain' }} />
                </View>
            )
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }} >
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                {this.renderToolbar()}

                {this.state.orderData == null || this.state.orderData == "" ?
                    null :

                    <>
                        {/* Image Back */}

                        <ImageBackground source={Images.info_background} style={{ width: '100%', height: 250 }}>
                            <View style={styles.viewImageBackground}>
                                <View style={styles.weekRow}>
                                    <Image style={styles.imageHash} source={Images.ic_hashtag}></Image>
                                    <TextBold title={strings.tv_order_id} textStyle={[styles.textOrder, { flex: 2 }]} />
                                    <TextBold title={this.state.order_number} textStyle={[styles.textOrder, { flex: 4 }]} />
                                </View>

                                <View style={styles.weekRow}>
                                    <Image style={{ width: 30, height: 30 }} source={Images.ic_hashtag}></Image>
                                    <TextBold title={strings.total} textStyle={[styles.textOrder, { flex: 2 }]} />
                                    <TextBold title={strings.currency_symbol + " " + this.state.orderData.amount} textStyle={[styles.textOrder, { flex: 4 }]} />
                                </View>
                            </View>
                        </ImageBackground>
                        {/* Status */}
                        {this.tracker(this.state.orderData.status)}

                        {/*Text of Status*/}

                        {parseInt(this.state.orderData.status) == 6 ?
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <TextBold title={strings.cancelled} textStyle={{ fontSize: 18 }} />
                            </View> :
                            <View style={{ flexDirection: "row", }}>
                                <TextBold title={strings.processing} textStyle={styles.tstatus} />
                                <TextBold title={strings.preparing} textStyle={styles.tstatus} />
                                <TextBold title={strings.ontheway} textStyle={styles.tstatus} />
                                <TextBold title={strings.delievered} textStyle={styles.tstatus} />
                            </View>
                        }
                        {/*Modify Order Buttons */}

                        <View style={{ flexDirection: "row", marginTop: 30, marginHorizontal: 25, justifyContent: 'flex-end' }}>
                            {parseInt(this.state.orderData.status) < 2 ?
                                <TouchableOpacity
                                    onPress={() => {
                                        this.editClicked()
                                    }}
                                    style={{ alignSelf: 'flex-start', marginLeft: 20, }}>
                                    <TextBold title={strings.edit} textStyle={{ color: Constants.color.primary, fontSize: 16 }} />
                                </TouchableOpacity>
                                : null}
                            <TouchableOpacity
                                onPress={() => {
                                    this.viewClicked()
                                }}
                                style={{ alignSelf: 'flex-start', marginLeft: 20, }}>
                                <TextBold title={strings.view} textStyle={{ color: Constants.color.primary, fontSize: 16 }} />
                            </TouchableOpacity>
                            {parseInt(this.state.orderData.status) < 2 ?
                                <TouchableOpacity
                                    onPress={() => {
                                        this.cancleClicked()
                                    }}
                                    style={{ alignSelf: 'flex-start', marginLeft: 20, }}>
                                    <TextBold title={strings.cancel} textStyle={{ color: Constants.color.black, fontSize: 16 }} />
                                </TouchableOpacity> : null}
                        </View>

                        {/*Driver Contact Details */}
                        {parseInt(this.state.orderData.status) == 4 ?
                            <View style={styles.driverContactCard}>
                                <TextBold title={strings.driver_contact_details} textStyle={styles.driverContactText} />
                                <View style={{ flexDirection: 'row', padding: 10, marginVertical: 10 }}>
                                    <View style={{ width: '25%', justifyContent: "center" }}>
                                        <Image source={
                                            this.state.orderData && this.state.orderData.driver && this.state.orderData.driver.profile_image && this.state.orderData.driver.profile_image != "" &&
                                                this.state.orderData && this.state.orderData.driver && this.state.orderData.driver.profile_image && this.state.orderData.driver.profile_image != null ?
                                                { uri: this.state.orderData.driver.profile_image }
                                                : Images.no_restaurant
                                        } style={styles.restImage} />
                                    </View>

                                    <View style={{ width: '55%', justifyContent: "center" }}>

                                        {this.state.orderData.driver
                                            && this.state.orderData.driver.first_name
                                            && !this.state.orderData.driver.last_name
                                            ? <TextBold textStyle={{ fontSize: 14, color: Constants.color.black }}
                                                title={this.state.orderData.driver.first_name} />
                                            : null}
                                        {this.state.orderData.driver
                                            && this.state.orderData.driver.first_name
                                            && this.state.orderData.driver.last_name
                                            ? <TextBold textStyle={{ fontSize: 14, color: Constants.color.black }}
                                                title={this.state.orderData.driver.first_name + " " +
                                                    this.state.orderData.driver.last_name} />
                                            : null}
                                        {!this.state.orderData.driver
                                            || (this.state.orderData.driver && !this.state.orderData.driver.first_name)
                                            ? <TextBold textStyle={{ fontSize: 14, color: Constants.color.black }}
                                                title={strings.name_not_found} />
                                            : null}
                                        {/* <TextBold textStyle={{ fontSize: 14, color: Constants.color.black }}
                                            title={this.state.orderData.driver.first_name + " " +
                                                this.state.orderData.driver.last_name} /> */}
                                        {this.state.orderData.driver
                                            && this.state.orderData.driver.contact_number
                                            ? <TextBold textStyle={{ fontSize: 14, color: Constants.color.black }}
                                                title={this.state.orderData.driver.contact_number} />
                                            : <TextBold textStyle={{ fontSize: 14, color: Constants.color.black }}
                                                title={strings.contact_not_found} />}

                                    </View>

                                    <View style={{ width: '20%', alignItems: "center", justifyContent: "center" }}>

                                        <TouchableOpacity
                                            onPress={() => {
                                                if (!this.state.orderData
                                                    || !this.state.orderData.driver
                                                    || !this.state.orderData.driver.contact_number
                                                    || this.state.orderData.driver.contact_number.toString().length == 0) {
                                                    alert(contact_not_valid)
                                                    return
                                                }

                                                this.callOnNumber(this.state.orderData.driver.contact_number)

                                                // if (Platform.OS === Constants.PLATFORM.ios) {
                                                //     RNImmediatePhoneCall.immediatePhoneCall(this.state.orderData.driver.contact_number)
                                                // } else {
                                                //     requestPhonePermission();
                                                //     if (PermissionCallAndroid) {
                                                //         RNImmediatePhoneCall.immediatePhoneCall(this.state.orderData.driver.contact_number)
                                                //     }
                                                // }
                                                // RNImmediatePhoneCall.immediatePhoneCall(this.state.driverPhone)
                                            }}
                                            style={styles.callImageView}>
                                            <Image source={Images.ic_phonecall_white} style={{ width: '50%', height: '50%' }} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            : null}
                    </>
                }
                {this.renderProgressBar()}
            </View>
        );

    }
    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>
            )
        } else {
            return
        }

    }

}

function mapStateToProps(state) {
    //console.log("BASKET_STATE:" + JSON.stringify(state.basketData))
    return {
        internet: state.internet,
    }
}

export default connect(mapStateToProps, {})(Order)







