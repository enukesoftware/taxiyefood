import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Platform, SafeAreaView, View, Image, Text, TouchableOpacity, Alert, AsyncStorage } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { clearLocation } from '../../redux/actions'
import NavigationService from '../../services/NavigationServices'
import { locationSelected ,isLoginAction,userDataAction,addItemInBasketAction, deviceTokenAction} from '../../redux/actions'

// import GlobalStyle from '../../../styles'

let headerLeft = '';

class SelectLocality extends Component {

    static navigationOptions = {
        header: null
    };

  

    // static navigationOptions = ({ navigation }) => {
    //     return {
    //       headerTitle: (
    //         <View style={{flex:1,height:'100%',backgroundColor:Constants.color.primary}}>
    //           <Text>title</Text>
    //         </View>
    //       )
    //     }
    //   }

    state = {
        address: strings.hint_location,
        isCloseVisible: false,
    }

    clearAddress = () => {
        // this.setState({
        //     address: strings.hint_location,
        //     isCloseVisible: false,
        // })
        this.props.clearLocation()
        this.setState({
            isCloseVisible: false
        })
    }

    useMyLocation = () => {
        this.props.navigation.navigate("SearchAddress");
    }

    constructor() {
        super()
        console.log("SELECT_ADDRESS");
    }

    componentDidMount() {
        //get last stored locaiton information
        AsyncStorage.getItem(Constants.STORAGE_KEY.selectedLocation, (error, result) => {
            if (error) {
                console.log("ERROR:" + JSON.stringify(error))
            }
            else {
                // this.setState({ userData: JSON.parse(result) })
                // console.warn(JSON.parse(result));
                if (result) {
                    console.log("RESULT:" + result)
                    result = JSON.parse(result)
                    this.props.locationSelected({
                        address: result.address,
                        latitude: result.latitude,
                        longitude: result.longitude,
                        addressToDisplay: result.addressToDisplay,
                        city: result.city
                    })
                    this.setState({
                        address: result.addressToDisplay,
                        isCloseVisible: true
                    })
                } else {
                    this.setState({
                        address: strings.hint_location,
                        isCloseVisible: false
                    })
                }
            }
        })

        //get last stored login status information
        AsyncStorage.getItem(Constants.STORAGE_KEY.isLogin, (error, result) => {
            if (error) {
                console.log("ERROR:" + JSON.stringify(error))
            }
            else {
                if (result) {
                    console.log("IS_LOGIN_RESULT:" + result)
                    result = JSON.parse(result)
                    this.props.isLoginAction(true)
                    this.setState({
                        address: result.addressToDisplay,
                        isCloseVisible: true
                    })
                }
            }
        })

        //get last stored user data information
        AsyncStorage.getItem(Constants.STORAGE_KEY.userData, (error, result) => {
            if (error) {
                console.log("ERROR:" + JSON.stringify(error))
            }
            else {
                if (result) {
                    console.log("USER_DATA_RESULT:" + result)
                    result = JSON.parse(result)
                    this.props.userDataAction(result)
                }
            }
        })

        //get last stored basket data information
        AsyncStorage.getItem(Constants.STORAGE_KEY.basketData, (error, result) => {
            if (error) {
                console.log("ERROR:" + JSON.stringify(error))
            }
            else {
                if (result) {
                    console.log("BASKET_DATA_RESULT:" + result)
                    result = JSON.parse(result)
                    this.props.addItemInBasketAction(result)
                }
            }
        })


        //get last stored device token information
        AsyncStorage.getItem(Constants.STORAGE_KEY.deviceToken, (error, result) => {
            if (error) {
                console.log("ERROR:" + JSON.stringify(error))
            }
            else {
                if (result) {
                    console.log("DEVICE_TOKEN_RESULT:" + result)
                    this.props.deviceTokenAction(result)
                }
            }
        })
        
    }


    renderCloseButton = () => {
        if (this.props.isCloseVisible) {
            return (
                <TouchableOpacity style={{ width: '10%' }} onPress={() => this.clearAddress()}>
                    <Image source={Images.ic_cross} style={{ width: 25, height: 25 }}></Image>
                </TouchableOpacity>
            )
        }
    }

    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }



    renderBackButton() {
        const { navigation } = this.props;
        const navigateFrom = navigation.getParam('NavigateFrom', '');
        if (navigateFrom === '' || navigateFrom === 'TutorialScreen') {
            return null
        }
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '20%'
                }
                }>
                    {this.renderBackButton()}


                </View>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: "center",
                    width: '60%'
                }
                }>
                    <TextBold title={strings.select_locality} textStyle={styles.textTitle} />
                </View>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '20%'
                }
                }>

                </View>
                {/* <View style={{ flexDirection: 'row',  width: '10%' }}>
                    <TouchableOpacity activeOpacity={0.8}
                    >
                        <Image source={Images.ic_close_white}
                            style={{ marginRight: 15, height: 25, width: 25 }}
                        ></Image>
                    </TouchableOpacity>
                </View> */}
            </View>

        )
    }

    showRestaurant = () => {
        if (this.props.address === strings.hint_location) {
            Alert.alert(strings.app_name, strings.search_selection_error, [{
                text: strings.ok
            }])
        } else if (this.props.city === '') {
            Alert.alert(strings.app_name, strings.area_selection_error, [{
                text: strings.ok
            }])
        } else {
            AsyncStorage.setItem(Constants.STORAGE_KEY.selectedLocation, JSON.stringify(this.props.location));
            NavigationService.navigate("Dashboard", { locationUpdate: this.props.location });
        }

    }

    render() {
        return (
            <View style={{ width: '100%', flex: 1 }}>
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                {this.renderToolbar()}
                <View style={styles.container}>


                    <Image source={Images.logo_search} style={styles.logoImageStyle}></Image>
                    <View style={styles.addressOuterViewStyle}>
                        <View style={styles.addressInnerViewStyle}>
                            <TextRegular title={this.props.address} numberOfLines={1} ellipsizeMode='tail' textStyle={{ paddingVertical: 5, width: '90%' }}></TextRegular>
                            {this.renderCloseButton()}
                        </View>
                    </View>
                    <View style={styles.orViewStyle}>
                        <View style={styles.dividerStyle}></View>
                        <TextRegular title={strings.or} textStyle={styles.orTextStyle}></TextRegular>
                        <View style={styles.dividerStyle}></View>
                    </View>

                    <TouchableOpacity
                        onPress={() => this.useMyLocation()}
                        style={{ marginTop: 15, flexDirection: 'row', alignItems: 'center' }}>
                        <Image source={Images.ic_my_location} style={{ width: 30, height: 30 }}></Image>
                        <TextBold title={strings.use_my_location_button} textStyle={{ color: Constants.color.primary, fontSize: Constants.fontSize.LargeX, marginLeft: 10 }}></TextBold>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.showRestaurant()}
                        style={{ width: '90%', marginTop: 20, backgroundColor: Constants.color.primary, borderRadius: 5, padding: 10, alignItems: 'center' }}>
                        <TextBold title={strings.show_restaurent_button} textStyle={{ color: 'white', padding: 10, fontSize: Constants.fontSize.NormalXX }} />
                    </TouchableOpacity>
                    <SafeAreaView style={{ flex: 1, backgroundColor: Constants.color.primary }}></SafeAreaView>
                </View>
            </View>
        );
    };
}

function mapStateToProps(state) {
    return {
        location: state.location,
        address: state.location.addressToDisplay,
        city: state.location.city,
        isCloseVisible: state.location.addressToDisplay === strings.hint_location ? false : true,
        isLogin:state.isLogin,
    }
}

export default connect(mapStateToProps, { clearLocation, locationSelected,isLoginAction,userDataAction,addItemInBasketAction,deviceTokenAction })(SelectLocality)