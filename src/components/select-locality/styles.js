import {Platform, StyleSheet } from 'react-native';
import { Constants } from '../../utils'


export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15,
        width:'100%'
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle: {
        fontSize: 20,
        color: Constants.color.white,
        marginLeft: 15,
    },
    logoImageStyle: {
        height: 100,
        width: 100,
        resizeMode: 'contain',
        marginTop: 40,
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    addressOuterViewStyle: {
        width: '90%',
        padding: 8,
        marginTop: 35,
        backgroundColor:Constants.color.dark_gray,
        borderRadius: 7
    },
    addressInnerViewStyle: { 
        paddingVertical: 10, 
        paddingLeft: 5, 
        backgroundColor: 'white', 
        borderRadius: 7 ,
        flexDirection:'row',
        alignItems:'center',
    },
    orViewStyle:{flexDirection:'row',justifyContent:'center',alignItems:'center',marginTop:15},
    dividerStyle:{height:1,width:'30%',backgroundColor:Constants.color.dark_gray},
    orTextStyle:{paddingBottom:5, marginHorizontal:6,fontSize:Constants.fontSize.LargeX,alignSelf:'center'},
})