import React, { Component } from 'react'
import { Platform, StyleSheet, View, SafeAreaView, Image, TouchableOpacity } from 'react-native';
// import styles from './styles'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Constants, strings, Images, Fonts } from '../../utils'
import { connect } from 'react-redux'
import { locationSelected } from '../../redux/actions'



class AddressAutocomplete extends Component {

    // static navigationOptions = {
    // headerRight: (
    //     <TouchableOpacity >
    //         <Image source={Images.ic_next_arrow}
    //             style={{ marginLeft: 15, height: 20, width: 20 }}
    //         ></Image>
    //     </TouchableOpacity>

    // )
    // };

    static navigationOptions = ({ navigation }) => ({
        header: null,

    });

    constructor() {
        super()
    }

    goBack = (address, location) => {
        const { navigation } = this.props;
        navigation.goBack();
        navigation.state.params.onSelect({
            address: address,
            addressForAutoSearch: address,
            region: {
                latitude: location.lat,
                longitude: location.lng
            }
        });
    }


    saveLocation = (address, location) => {
        this.props.locationSelected({ address: address, latitude: location.latitude, longitude: location.longitude })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ backgroundColor: Constants.color.primaryDark }} />
                <View style={{ flex: 1 }}>
                    <View
                        style={{
                            width: '100%',
                            height:'100%',
                            shadowColor: '#000',
                            shadowOffset: { width: 0, height: 2 },
                            shadowOpacity: 0.2,
                            shadowRadius: 2,
                            elevation: 8,
                        }}>
                        
                        <GooglePlacesAutocomplete
                            placeholder='Enter Location'
                            minLength={2}
                            autoFocus={false}
                            returnKeyType={'search'}
                            fetchDetails={true}
                            placeholder={strings.menu_search}
                            placeholderTextColor="white"
                            query={{
                                // available options: https://developers.google.com/places/web-service/autocomplete
                                key: Constants.GOOGLE_API_KEY,
                                language: 'en', // language of the results
                                types: 'geocode'
                            }}
                            // textInputProps={{
                            //     clearButtonMode: 'never',
                            //     ref: input => {
                            //         this.textInput = input;
                            //     }
                            // }}
                            onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                console.log('DATA ' + JSON.stringify(data.description));
                                console.log('DETAILS ' + JSON.stringify(details.geometry.location));
                                this.goBack(data.description, details.geometry.location)
                                // this.saveLocation(data.description, details.geometry.location)
                            }}
                            // renderRightButton={() => (
                            //     <TouchableOpacity
                            //         // style ={styles.clearButton}
                            //         onPress={() => {
                            //             this.textInput.clear();
                            //         }}
                            //     >
                            //         <Image source={Platform.OS == 'ios' ? Images.ic_back_ios : Images.ic_back_android} style={{backgroundColor: Constants.color.primary, width: 44, height:44 }}></Image>

                            //     </TouchableOpacity>
                            // )}
                            // renderLeftButton={() => (
                            //     <TouchableOpacity
                            //         // style ={styles.clearButton}
                            //         onPress={() => {
                            //             this.textInput.clear();
                            //         }}
                            //     >
                            //         <Image source={Platform.OS == 'ios' ? Images.ic_back_ios : Images.ic_back_android} style={{backgroundColor: Constants.color.primary, width: 44, height:44 }}></Image>

                            //     </TouchableOpacity>
                            // )}
                            styles={{
                                textInputContainer: {
                                    borderTopWidth: 0,
                                    borderBottomWidth: 0,
                                    paddingLeft:44,
                                    backgroundColor:Constants.color.primary,
                                },
                                textInput: {
                                    // marginLeft: 0,
                                    // marginRight: 0,
                                    height: 44,
                                    // width: '100%',
                                    // color: '#5d5d5d',
                                    color: 'white',
                                    fontSize: Constants.fontSize.NormalXX,
                                    backgroundColor: Constants.color.primary,
                                    //  margin:0
                                    borderRadius: 0,
                                    paddingTop: 0,
                                    paddingBottom: 0,
                                    paddingLeft: 10,
                                    paddingRight: 10,
                                    marginTop: 0,
                                    marginLeft: 0,
                                    marginRight: 0,
                                    fontFamily: Fonts.Regular
                                },
                                predefinedPlacesDescription: {
                                    // color: '#1faadb'
                                    color: 'white',
                                    fontFamily: Fonts.Regular
                                },
                            }}
                            currentLocation={false}
                        />
                        <TouchableOpacity
                            onPress={()=>this.props.navigation.goBack()}
                            style={{
                                backgroundColor: Constants.color.primary,
                                width: 44,
                                height: 44,
                                alignItems: 'flex-start',
                                justifyContent: 'center',
                                paddingLeft:4,
                                position:'absolute',
                                top:0,
                                left:0,
                            }}>
                            <Image source={Platform.OS == 'ios' ? Images.ic_back_ios : Images.ic_back_android} style={{ width: 24, height: 24 }}></Image>

                        </TouchableOpacity>
                    </View>

                </View>
            </View>

        );
    };
}

function mapStateToProps(state) {
    return {
        location: state
    }
}

export default connect(mapStateToProps, { locationSelected })(AddressAutocomplete)