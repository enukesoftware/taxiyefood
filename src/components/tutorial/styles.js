import { StyleSheet } from 'react-native';
import {Constants} from '../../utils'


export default StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: 'white'
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 20,
        justifyContent:'center'
    },
    ButtonContainer:{
        backgroundColor:'red',
    },
    nextArrowStyle:{
        width:16,
        height:16,
        resizeMode:'contain',
        marginLeft:10,
        alignSelf:'center'
    }
})