import React, { Component } from 'react'
import { Platform, TouchableOpacity, ActivityIndicator, Image, View, ImageBackground, Text, } from 'react-native';
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import styles from './styles'
import { connect } from 'react-redux'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { OrderDetailApi } from '../../services/APIService'
import { SafeAreaView } from 'react-navigation';
import NavigationService from '../../services/NavigationServices'
import { ScrollView } from 'react-native-gesture-handler';



class ViewOrder extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            orderData: null,
            status: 1,
            order_number: "",
            totalamount: "$250.00",
            subtotal: '$220',
            deliveryFee: '$20.00',
            packingCharges: '$10.00',
            orderDetails: [
                {
                    amount: 1,
                    itemName: 'Biryani Rice',
                    oneitemPrice: '$100',
                    totalitemPrice: '$100',
                },
                {
                    amount: 2,
                    itemName: 'Chapati',
                    oneitemPrice: '$60',
                    totalitemPrice: '$120',
                }
            ],


        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        const navigateFrom = navigation.getParam('NavigateFrom', '');

        //console.log('addessJson:'+navigateFrom)
        if (navigateFrom == 'Order') {
            const order_number = navigation.getParam('order_number', '');
            if (order_number != "" || order_number != null) {
                this.setState({
                    order_number: order_number
                })
                this.callOrderDetailApi(order_number)
            }
        }
        /* this.setState({
            order_number: 1564990623 // 1565070294

        })
        this.callOrderDetailApi(1564990623) */
    }

    calculateStatus(id, type) {
        switch (parseInt(id)) {
            case 0:
                if (type === 0) return Images.ic_check_white
                return strings.processing
            case 1:
                if (type === 0) return Images.ic_check_white
                return strings.processing
            case 2:
                if (type === 0) return Images.ic_preparing_enabled
                return strings.preparing
            case 3:
                if (type === 0) return Images.ic_preparing_enabled
                return strings.preparing
            case 4:
                if (type === 0) return Images.ic_distance_location_white
                return strings.ontheway
            case 5:
                if (type === 0) return Images.ic_delivery_man_white
                return strings.delievered
            case 6:
                if (type === 0) return Images.cancel
                return strings.cancelled
            case 7:
                if (type === 0) return Images.ic_check_white
                return strings.confirmed
        }
    }

    callOrderDetailApi = (order_number) => {
        if(!this.props.internet){
                    alert(strings.message_lno_internet)
                    return
                }
        this.setState({
            isLoading: true,
        })

        let param = {
            order_number: order_number,
        }

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.orderDetail

        OrderDetailApi(url, param).then(res => {


            if (res && res.success) {
                console.log("ORDER-DETAIL:" + JSON.stringify(res))
                this.setState({
                    isLoading: false,
                    orderData: res.data
                })
                // alert(res.message);
            } else {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res && res.error) {
                        alert(res.error)
                    }
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err) + "catch");
                }
            }, 100);
        });

    }

    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }




    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }

    renderTotalPrice() {
        return (
            <View style={{ flexDirection: "row", marginHorizontal: 35, paddingVertical: 15 }}>
                <View style={{ width: '50%', justifyContent: 'flex-end', flexDirection: "row", }}>
                    <TextRegular title={strings.total} textStyle={{ color: 'black', fontSize: 22 }}></TextRegular>
                </View>
                <View style={{ width: '20%', justifyContent: 'flex-end', flexDirection: "row", }}>

                </View>
                <View style={{ width: '30%', flexDirection: "row", }}>
                    <TextRegular title={strings.currency_symbol + this.state.orderData.amount}
                     textStyle={{ color: 'black', fontSize: 22 }}></TextRegular>
                </View>
            </View>

        )
    }
    calculateSubtotal() {
        const data = this.state.orderData
        const discount = parseFloat(data.discount)
        const amount = parseFloat(data.amount)
        const shipping_charge = parseFloat(data.shipping_charge)
        const packaging_fees = parseFloat(data.packaging_fees)
        const cgst = parseFloat(data.cgst)
        const sgst = parseFloat(data.sgst)
        return (
            amount - (cgst + sgst + packaging_fees + shipping_charge) + discount
        )
    }
    calculateTotal() {
        const data = this.state.orderData
        const amount = parseFloat(data.amount)
        const shipping_charge = parseFloat(data.shipping_charge)
        const packaging_fees = parseFloat(data.packaging_fees)
        const cgst = parseFloat(data.cgst)
        const sgst = parseFloat(data.sgst)
        return (
            amount - (cgst + sgst + packaging_fees + shipping_charge)
        )
    }


    renderTotalPriceList() {
        let b = [
            { k: strings.subtotal, v: strings.currency_symbol + this.calculateSubtotal() },
            { k: strings.discount, v: strings.currency_symbol + this.state.orderData.discount },
            { k: strings.total, v: strings.currency_symbol + this.calculateTotal() },
            { k: strings.delivery_fee, v: strings.currency_symbol + this.state.orderData.shipping_charge },
            { k: strings.packing_charge, v: strings.currency_symbol + this.state.orderData.packaging_fees },
            { k: strings.cgst, v: strings.currency_symbol + this.state.orderData.cgst },
            { k: strings.sgst, v: strings.currency_symbol + this.state.orderData.sgst },

        ]
        return (
            b.map((item, index) => (
                <View key={index} style={styles.itemOrderView}>
                    <View style={{ width: '50%', justifyContent: 'flex-end', flexDirection: "row", }}>
                        <TextRegular title={item.k} textStyle={{ color: 'black', fontSize: 16 }}></TextRegular>
                    </View>
                    <View style={{ width: '20%', justifyContent: 'flex-end', flexDirection: "row", }}>

                    </View>
                    <View style={{ width: '30%', flexDirection: "row", }}>
                        <TextRegular title={item.v} textStyle={{ color: 'gray', fontSize: 16 }}></TextRegular>
                    </View>
                </View>
            ))
        )
    }

    calculateItemPrice(item) {
        const unitP = parseInt(item.product_unit_price)
        const quan = parseInt(item.product_quantity)
        addonPrice = 0
        let arr = []
        arr = item.addons_list
        if (arr != null) {
            for (let i = 0; i < arr.length; i++) {
                addonPrice = addonPrice + parseInt(item.addons_list[i].price)
            }
        }
        return quan * (unitP - addonPrice)
    }

    renderItemOrder() {
        return (
            this.state.orderData.items.map((item, index) => (
                <View key={index}>
                    <View style={{ height: 2, width: '100%', backgroundColor: Constants.color.gray, marginVertical: 15 }}></View>
                    <View style={styles.itemOrderView}>
                        <View style={{ width: '20%',alignItems:'center' }}>
                        <Image source={(item.image && item.image.location && item.image.location != "") ? { uri: item.image.location } : Images.no_product} style={[styles.menuInfoImage,{paddingHorizontal:10}]}></Image>

                        </View>
                        <View style={{ width: '45%', flexDirection: "row", alignItems: 'center',marginRight:'5%',paddingRight:10 }}>
                            <TextRegular title={(item.product_quantity) + ' x '} textStyle={{ color: 'green', fontSize: 18 }}></TextRegular>
                            <TextRegular title={item.product_name } textStyle={{ color: 'black', fontSize: 15 }}></TextRegular>
                        </View>
                        <View style={{ width: '30%', flexDirection: "row", alignItems: 'center' }}>
                            <TextRegular title={strings.currency_symbol + this.calculateItemPrice(item)} textStyle={{ color: 'gray', fontSize: 15 }}></TextRegular>
                        </View>
                    </View>

                    {item.addons_list == null ? null : <View>
                        <View style={styles.itemOrderView}>
                            <View style={{ width: '20%', flexDirection: "row", }}></View>
                            <TextRegular title={strings.AddOn} textStyle={{ marginTop: 5, fontSize: 16, color: Constants.color.black }} />
                        </View>
                        {this.renderAddOn(item.addons_list, item.product_quantity)}


                    </View>}
                </View>
            ))
        )
    }

    renderAddOn(addons_list, quantity) {
        return (
            addons_list.map((item, index) => (
                <View key={index} >
                    <View style={styles.itemOrderView}>
                        <View style={{ width: '25%', flexDirection: "row", }}></View>
                        <View style={{ width: '40%', flexDirection: "row", alignItems: 'center',marginRight:'5%',paddingRight:10 }}>
                            <TextRegular title={(quantity) + ' x '} textStyle={{ color: 'green', fontSize: 18 }}></TextRegular>
                            <TextRegular title={item.item_name} textStyle={{ color: 'black', fontSize: 15 }}></TextRegular>
                        </View>
                        <View style={{ width: '30%', }}>
                            <TextRegular title={strings.currency_symbol +
                                ((parseInt(item.price)) * (parseInt(quantity)))} textStyle={{ color: 'gray', fontSize: 15 }}></TextRegular>
                        </View>
                    </View>
                </View>
            )
            )
        )
    }

    renderOrderMain() {
        let a = [
            { img: Images.ic_hashtag, string: strings.tv_order_id, datamain: this.state.order_number },
            { img: Images.ic_tickets, string: strings.total, datamain: strings.currency_symbol + this.state.orderData.amount }
        ]
        return (
            a.map((item, index) => (
                < View key={index} style={{ flexDirection: 'row', marginHorizontal: 20, paddingVertical: 10, width: '100%', alignItems: 'center' }}>
                    <View style={{ width: '10%', alignItems: 'center' }}>
                        <Image style={{
                            width: 30,
                            height: 30,
                            resizeMode: 'contain'
                        }} source={item.img} />
                    </View>
                    <View style={{ width: '30%', alignItems: 'center' }}>
                        <TextRegular textStyle={{
                            color: 'black',
                            fontSize: 18,
                            marginHorizontal: 15
                        }} title={item.string} />
                    </View>
                    <View style={{ width: '10%', alignItems: 'center' }}>
                        <TextBold textStyle={{
                            color: 'black',
                            fontSize: 18,
                            marginHorizontal: 15
                        }} title={':'} />
                    </View>
                    <View style={{ width: '50%', alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                        <TextRegular textStyle={{
                            color: 'black',
                            fontSize: 18,
                            marginHorizontal: 15
                        }} title={item.datamain} />
                    </View>
                </View >
            ))

        )
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={strings.view_order} textStyle={styles.textTitle} />

                </View>

            </View>

        )
    }


    render() {

        return (
            <View style={{ flex: 1 }} >
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                {this.renderToolbar()}
                {this.state.orderData == null || this.state.orderData == "" ?
                    null :
                    <ScrollView >
                        {/* Image Back */}
                        <ImageBackground source={Images.info_background} style={{ width: '100%', height: 180, alignItems: 'center', }}>
                            <TextRegular title={strings.status} textStyle={{ color: Constants.color.white, fontSize: 20, marginTop: 30, }} />
                            <View style={styles.processView}>
                                <Image style={styles.img} source={this.calculateStatus(this.state.orderData.status, 0)} />
                                <TextBold textStyle={{
                                    color: 'white', fontSize: 16, marginHorizontal: 10
                                }} title={(this.calculateStatus(this.state.orderData.status, 1)).toUpperCase()} />
                            </View>
                        </ImageBackground>
                        {/* View Details*/}
                        <View style={{ paddingVertical: 20 }}>
                            {this.renderOrderMain()}
                        </View>
                        <TextBold title={strings.tv_item_order} textStyle={styles.itemOrderText} />

                        {this.renderItemOrder()}
                        <View style={{ height: 2, width: '100%', backgroundColor: Constants.color.gray, marginVertical: 25 }}></View>

                        {this.renderTotalPriceList()}

                        {this.renderTotalPrice()}

                    </ScrollView>
                }{this.renderProgressBar()}
            </View>
        );

    }
    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>
            )
        } else {
            return
        }

    }

}


function mapStateToProps(state) {
    
    return {
        internet:state.internet,
    }
}

export default connect(mapStateToProps, {})(ViewOrder)






