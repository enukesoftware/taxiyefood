import ActionTypes from './types'

export const internetConnected = () =>{
    console.log('ACTION_CONNECTED')
    return{
        type:ActionTypes.INTERNET_CONNECTED
    };
}

export const internetDisconnected = () =>{
    console.log('ACTION_DISCONNECTED')
    return{
        type:ActionTypes.INTERNET_DISCONNECTED
    };
}