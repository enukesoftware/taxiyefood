import ActionTypes from './types'

export const dashboardTabSelected = (selected) =>{
    return{
        type:ActionTypes.DASHBOARD_TAB_SELECTED,
        dashboardTabSelected:selected
    };
}
