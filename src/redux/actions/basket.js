import ActionTypes from './types'

export const addItemInBasketAction = (item) =>{
    return{
        type:ActionTypes.ADD_ITEM_IN_BASKET,
        item:item
    };
}

export const removeItemFromBasketAction = (item) =>{
    return{
        type:ActionTypes.REMOVE_ITEM_FROM_BASKET,
        item:item
    };
}                        

export const getBasketDataAction = () =>{
    return{
        type:ActionTypes.BASKET_DATA,
    };
}

// export const clearBasketDataAction = () =>{
//     return{
//         type:ActionTypes.BASKET_DATA,
//     };
// }