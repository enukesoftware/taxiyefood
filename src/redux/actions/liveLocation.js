import ActionTypes from './types'

export const liveLocationAction = (liveLocationData) =>{
    return{
        type:ActionTypes.LIVE_LOCATION,
        liveLocationData:liveLocationData
    };
}
