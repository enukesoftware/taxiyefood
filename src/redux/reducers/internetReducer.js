import ActionTypes from '../actions/types'

export default (state = false, action) => {
    switch (action.type) {
        case ActionTypes.INTERNET_CONNECTED:
            console.log('INTERNET:CONNECT')
            return state = true
        case ActionTypes.INTERNET_DISCONNECTED:
            console.log('INTERNET:DISCONNECT')
            return state = false
        default:
            console.log('INTERNET:DEFAULT')
            return state
    }
}