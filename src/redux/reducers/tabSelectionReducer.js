import ActionTypes from '../actions/types'
import {strings} from '../../utils'

export const dashboardTabSelectedReducer = (state = 0, action) => {
    switch (action.type) {
        case ActionTypes.DASHBOARD_TAB_SELECTED:
            return action.dashboardTabSelected
        default:
            return state
    }
}